lyr.stats = {max:255.144226074,min:0.0};
lyr.m[0] = {i:0,type:1,ds:1};

// Layer 1
lyr = project.addLayer(new Q3D.PointLayer({q:1,objType:"COLLADA model",type:"point",name:"TubalanIPGEO"}));
lyr.a = ["Name", "PhotoLink"];

for (var idx in mapData) {
    var pointData = {
        scale: 0.019460909736,
        rotateX:0.0,
        rotateY:0.0,
        rotateZ:0.0,
        model_index:0,
        pts:[mapData[idx].points]
    }
    lyr.f.push(pointData);
    lyr.f[idx].a = [
        mapData[idx].name,
        '<a href="/static/img/houses/IMG_20161116_150516-1.jpg" target="_blank"><img src="/static/img/houses/IMG_20161116_150516-1.jpg" width="200px"/></a>'
    ];
}
