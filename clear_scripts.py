from fisheries.models import (
    FamilyPond,
    FARInventory,
)

family_ponds = FamilyPond.objects.filter(
    pond_areas__isnull=True,
    fish_captures__isnull=True,
    vessel='',
    vessel_notes='',
    registration_notes='',
    fishing_gears__isnull=True,
    other_fishing_gears='',
)

count = 0
for family_pond in family_ponds:
    pond = family_pond.pond_set.first()
    if pond is None:
        count += 1
    elif any([
        pond.area_developed,
        pond.cultured_species,
        pond.total_harvest,
        pond.volume_sold,
        pond.total_income,
        pond.total_expenses
    ]):
        pond.delete()
