from django.db import models


class GrossMonthlyIncome(models.Model):
    order = models.IntegerField(blank=True, null=True)
    value = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'gross monthly income'
        verbose_name_plural = 'gross monthly incomes'

    def __str__(self):
        return self.value


class FamilyIncomeQuerySet(models.QuerySet):

    def without_income(self, **kwargs):
        return self.filter(gross_monthly_income__isnull=True, **kwargs)

    def with_income(self, **kwargs):
        return self.filter(gross_monthly_income__isnull=False, **kwargs)


class FamilyIncome(models.Model):
    family = models.OneToOneField('families.Family', on_delete=models.CASCADE)
    farming = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    fishing = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    government_employment = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    private_employment = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    ofw_remittances = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    pension = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    subsidies = models.DecimalField('from 4Ps / Subsidies', max_digits=12, decimal_places=2, blank=True, null=True)
    self_employment = models.DecimalField('from self employment', max_digits=12, decimal_places=2, blank=True, null=True)
    business = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    other_sources = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    other_income_sources = models.CharField(max_length=200, blank=True)
    gross_monthly_income = models.ForeignKey('GrossMonthlyIncome', blank=True, null=True)

    objects = FamilyIncomeQuerySet.as_manager()

    class Meta:
        verbose_name = 'family income'
        verbose_name_plural = 'family incomes'

    def __str__(self):
        return str(self.family)


class FamilyExpenditures(models.Model):
    family = models.OneToOneField('families.Family', on_delete=models.CASCADE)
    food = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    education = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    medicine = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    power_bill = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    water_bill = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    philhealth = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    sss = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    pag_ibig = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    gsis = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    clothing = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    insurance = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    vehicle = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    taxes = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    debt_repayments = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    house_rental = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    house_helper = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    vices = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    others = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    other_expenditures = models.CharField(max_length=200, blank=True)

    class Meta:
        verbose_name = 'family expenditure'
        verbose_name_plural = 'family expenditures'

    def __str__(self):
        return str(self.family)


class PersonalProperty(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'personal property'
        verbose_name_plural = 'personal properties'

    def __str__(self):
        return self.name


class RadioProgram(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'radio program / station'
        verbose_name_plural = 'radio programs / stations'

    def __str__(self):
        return self.name


class CellularNetwork(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'cellular network'
        verbose_name_plural = 'cellular networks'

    def __str__(self):
        return self.name


class InternetProvider(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'internet provider'
        verbose_name_plural = 'internet providers'

    def __str__(self):
        return self.name


class FamilyAssets(models.Model):
    family = models.OneToOneField('families.Family', on_delete=models.CASCADE)
    monthly_bank_savings = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    monthly_cooperative_savings = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    monthly_others_savings = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    personal_properties = models.ManyToManyField('PersonalProperty')

    class Meta:
        verbose_name = 'family asset'
        verbose_name_plural = 'family assets'

    def __str__(self):
        return str(self.family)


class FamilyAccess(models.Model):
    family = models.OneToOneField('families.Family', on_delete=models.CASCADE)
    radio_programs = models.ManyToManyField('RadioProgram')
    cellular_networks = models.ManyToManyField('CellularNetwork')
    internet_providers = models.ManyToManyField('InternetProvider')
    local_newspaper = models.CharField(max_length=200, blank=True)
    national_newspaper = models.CharField(max_length=200, blank=True)
    other_access = models.CharField(max_length=200, blank=True)
    access_frequency = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = 'family access to communication'
        verbose_name = 'family access to communications'

    def __str__(self):
        return str(self.family)
