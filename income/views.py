from rest_framework import (
    viewsets,
)

from .models import (
    FamilyIncome,
    FamilyExpenditures,
    FamilyAssets,
    FamilyAccess,
)
from .serializers import (
    FamilyIncomeSerializer,
    FamilyExpendituresSerializer,
    FamilyAssetsSerializer,
    FamilyAccessSerializer,
)


class FamilyIncomeViewSet(viewsets.ModelViewSet):
    queryset = FamilyIncome.objects.all()
    serializer_class = FamilyIncomeSerializer


class FamilyExpendituresViewSet(viewsets.ModelViewSet):
    queryset = FamilyExpenditures.objects.all()
    serializer_class = FamilyExpendituresSerializer


class FamilyAssetsViewSet(viewsets.ModelViewSet):
    queryset = FamilyAssets.objects.all()
    serializer_class = FamilyAssetsSerializer


class FamilyAccessViewSet(viewsets.ModelViewSet):
    queryset = FamilyAccess.objects.all()
    serializer_class = FamilyAccessSerializer
