from django.contrib import admin

from .models import (
    FamilyAccess,
    FamilyAssets,
    FamilyExpenditures,
    FamilyIncome,
    GrossMonthlyIncome,
)


admin.site.register(FamilyAccess)
admin.site.register(FamilyAssets)
admin.site.register(FamilyExpenditures)
admin.site.register(FamilyIncome)
admin.site.register(GrossMonthlyIncome)
