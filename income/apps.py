from django.apps import AppConfig


class IncomeAndExpendituresConfig(AppConfig):
    name = 'income_and_expenditures'
