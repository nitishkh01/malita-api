from rest_framework import serializers

from .models import (
    FamilyAccess,
    FamilyAssets,
    FamilyExpenditures,
    FamilyIncome,
    PersonalProperty,
    RadioProgram,
    CellularNetwork,
    InternetProvider,
)


class FamilyIncomeSerializer(serializers.ModelSerializer):
    gross_monthly_income = serializers.SerializerMethodField()

    class Meta:
        model = FamilyIncome
        fields = '__all__'

    def get_gross_monthly_income(self, obj):
        return obj.gross_monthly_income.value if obj.gross_monthly_income else None


class FamilyExpendituresSerializer(serializers.ModelSerializer):

    class Meta:
        model = FamilyExpenditures
        fields = '__all__'


class PersonalPropertySerializer(serializers.ModelSerializer):

    class Meta:
        model = PersonalProperty
        fields = '__all__'


class FamilyAssetsSerializer(serializers.ModelSerializer):
    personal_properties = PersonalPropertySerializer(many=True, read_only=True)

    class Meta:
        model = FamilyAssets
        fields = '__all__'


class RadioProgramSerializer(serializers.ModelSerializer):

    class Meta:
        model = RadioProgram
        fields = '__all__'


class CellularNetworkSerializer(serializers.ModelSerializer):

    class Meta:
        model = CellularNetwork
        fields = '__all__'


class InternetProviderSerializer(serializers.ModelSerializer):

    class Meta:
        model = InternetProvider
        fields = '__all__'


class FamilyAccessSerializer(serializers.ModelSerializer):
    radio_programs = RadioProgramSerializer(many=True, read_only=True)
    cellular_networks = CellularNetworkSerializer(many=True, read_only=True)
    internet_providers = InternetProviderSerializer(many=True, read_only=True)

    class Meta:
        model = FamilyAccess
        fields = '__all__'
