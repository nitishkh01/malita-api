from rest_framework import (
    viewsets,
)

from .models import (
    NutritionProfile,
)
from .serializers import (
    NutritionProfileSerializer,
)


class NutritionProfileViewSet(viewsets.ModelViewSet):
    queryset = NutritionProfile.objects.all()
    serializer_class = NutritionProfileSerializer
