from django.db import models

from families.constants import (
    PHILHEALTH_CHOICES
)


class Sickness(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'sickness'
        verbose_name_plural = 'sicknesses'

    def __str__(self):
        return self.name


class NutritionProfile(models.Model):
    family = models.OneToOneField('families.Family', related_name='nutrition')
    has_newborn_estimated_2_5 = models.NullBooleanField(
        blank=True, null=True, help_text='Has newborn with estimated birth weight of 2.5 kgs'
    )
    num_newborn_2_5 = models.PositiveIntegerField(
        blank=True, null=True, help_text='Number of Newborn with estimated weight of 2.5kg'
    )
    has_newborn_below_2_5 = models.NullBooleanField(
        blank=True, null=True, help_text='Has newborn with birth weight below 2.5k'
    )
    num_newborn_below_2_5 = models.PositiveIntegerField(
        blank=True, null=True, help_text='No. of Newborn below 2.5kg'
    )
    has_children_immunized = models.NullBooleanField(
        blank=True, null=True, help_text='Children Immunized?'
    )
    ages_last_immunization = models.TextField(
        blank=True, help_text='Ages of children on last immunization'
    )
    has_malnourished_children = models.NullBooleanField(
        blank=True, null=True, help_text='Has children (2-6 years old) identified as malnourished'
    )
    malnourished_notes = models.TextField(blank=True, help_text='Notes on Malnourished Children')
    has_pregnancy_supplement = models.NullBooleanField(
        blank=True, null=True,
        help_text='Has pregnant and lactating mothers provided with iron and iodine supplement'
    )
    pregnancy_supplement = models.TextField(blank=True)
    has_prenatal_services = models.NullBooleanField(
        blank=True, null=True, help_text='Has pregnant mothers availing Pre-natal Services'
    )
    prenatal_services = models.TextField(blank=True)
    has_trained_delivery = models.NullBooleanField(
        blank=True, null=True,
        help_text='Deliveries attended by trained personnel (doctor, midwives, nurses, etc)'
    )
    deliveries_notes = models.TextField(blank=True)
    has_death_below_1_year_old = models.NullBooleanField(
        blank=True, null=True, help_text='Has death occurences of babies below 1 year old'
    )
    death_occurence_notes = models.TextField(blank=True)
    children_sicknesses = models.ManyToManyField(
        'Sickness', related_name='child_nutrition_profiles',
        help_text='Common sickness inflicting infants and children'
    )
    children_sicknesses_other = models.CharField(
        max_length=100, blank=True, help_text='Other common sickness among children'
    )
    adults_sicknesses = models.ManyToManyField(
        'Sickness', related_name='adult_nutrition_profiles',
        help_text='Common sickness inflicting adults'
    )
    adults_sicknesses_other = models.CharField(
        max_length=100, blank=True, help_text='Other common sickness inflicting adults'
    )
    philhealth_membership = models.CharField(max_length=100, blank=True, default='', choices=PHILHEALTH_CHOICES)

    class Meta:
        verbose_name = 'Health and Nutrition Profile'
        verbose_name_plural = 'Health and Nutrition Profile'

    def __str__(self):
        return str(self.family)
