from django.contrib import admin

from .models import (
    NutritionProfile,
    Sickness,
)


admin.site.register(NutritionProfile)
admin.site.register(Sickness)
