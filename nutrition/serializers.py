from rest_framework import serializers

from .models import (
    NutritionProfile,
)


class NutritionProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = NutritionProfile
        fields = '__all__'
