from rest_framework import (
    viewsets,
)

from .models import (
    EducationalProfile,
)
from .serializers import (
    EducationalProfileSerializer,
)


class EducationalProfileViewSet(viewsets.ModelViewSet):
    queryset = EducationalProfile.objects.all()
    serializer_class = EducationalProfileSerializer
