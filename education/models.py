from django.db import models


class EducationalProfile(models.Model):
    family = models.OneToOneField('families.Family', related_name='education')
    children_in_daycare = models.PositiveIntegerField(
        blank=True, null=True,
        help_text='No. of children (2-4 years old) in Day Care'
    )
    children_in_kinder = models.PositiveIntegerField(
        blank=True, null=True,
        help_text='No. of children (5-year old) in kindergarten'
    )
    has_children_in_elementary = models.NullBooleanField(
        blank=True, null=True,
        help_text='Has children 6-12 years old in Elementary School'
    )
    has_members_able_to_read_and_write = models.NullBooleanField(
        blank=True, null=True,
        help_text='Has family members (10 years and above) able to read and write, and do simple calculations'
    )
    has_members_able_to_vote = models.NullBooleanField(
        blank=True, null=True,
        help_text='Has family members able to vote at elections'
    )
    has_members_able_to_attend_barangay_assembly = models.NullBooleanField(
        blank=True, null=True,
        help_text='Has family members able to attend barangay assembly, PTCA meetings, Purok Meetings, etc',
    )
    children_not_in_elementary = models.PositiveIntegerField(
        help_text='No. of children (6-12 years old) not in elementary', blank=True, null=True
    )
    children_not_in_junior_hs = models.PositiveIntegerField(
        help_text='No. of children not in Junior High School', blank=True, null=True
    )
    children_not_in_senior_hs = models.PositiveIntegerField(
        help_text='No. of children not in Senior High School', blank=True, null=True
    )
    children_not_in_college = models.PositiveIntegerField(
        help_text='No. of children not in College', blank=True, null=True
    )
    children_college_graduate = models.PositiveIntegerField(
        help_text='No. of children who Graduated in College', blank=True, null=True
    )
    children_college_level = models.PositiveIntegerField(
        help_text='No. of children at College Level', blank=True, null=True
    )
    children_high_school_graduate = models.PositiveIntegerField(
        help_text='No. of children who are High School graduate', blank=True, null=True
    )
    children_high_school_level = models.PositiveIntegerField(
        help_text='No. of children who are High School Level', blank=True, null=True
    )
    children_elementary_graduate = models.PositiveIntegerField(
        help_text='No. of children who are Elementary Graduate', blank=True, null=True
    )
    children_elementary_level = models.PositiveIntegerField(
        help_text='No. of children who are Elementary Level', blank=True, null=True
    )

    class Meta:
        verbose_name = 'educational profile'
        verbose_name_plural = 'educational profiles'

    def __str__(self):
        return str(self.family)
