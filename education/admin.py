from django.contrib import admin

from .models import EducationalProfile


admin.site.register(EducationalProfile)
