from rest_framework import serializers

from .models import (
    EducationalProfile,
)


class EducationalProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = EducationalProfile
        fields = '__all__'
