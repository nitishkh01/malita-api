#!/bin/bash
python check_db.py
python manage.py migrate
python manage.py collectstatic --no-input
python manage.py shell < scripts/populate_points.py

# Prepare log files and start outputting logs to stdout
touch /srv/logs/gunicorn.log
touch /srv/logs/access.log

# Start Gunicorn processes
echo Starting Gunicorn.
exec gunicorn profiling.wsgi:application \
     --name profiling \
     --bind 0.0.0.0:8000 \
     --workers 3
