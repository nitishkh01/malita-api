import os
import decimal
import scrapy


from django.conf import settings
from django.core.exceptions import FieldError
from django.db import transaction
from dateutil import parser

from agriculture.models import (
    Animal,
    Breed,
    Crop,
    CropType,
    FARM_TYPE_CHOICES,
    FamilyCrop,
    FamilyCropInfo,
    FamilyFarm,
    FamilyLiveStock,
    FamilyPetAndPoultry,
    FarmLabor,
    FarmMachinery,
    FarmMarketing,
    FarmWastePractice,
    FarmWorker,
    FarmerOrganization,
    FarmingPractice,
    FarmingProblem,
    FinancingPractice,
    LiveStock,
    MarketInfo,
    MarketOutlet,
    OrgThought,
    Poultry,
    ProductionEconomy,
    SeedType,
    SellingPrice,
    TenurialInstrument,
)
from education.models import EducationalProfile
from families.models import (
    Family,
    FamilyProfession,
    Head,
    LGU,
    Spouse,
    PsychoSocial,
    Disability,
    ServiceGrant,
)
from fisheries.models import (
    FamilyPond,
    PondArea,
    Pond,
    FishCapture,
    FishingGear,
    FARInventory,
    FishAndAquatic,
)
from housing.models import (
    BuildingElectricity,
    BuildingType,
    DrainageType,
    FamilyHousing,
    GarbageDisposal,
    HouseLocationRisk,
    ProductTransportation,
    RoofMaterial,
    ToiletType,
    WallingMaterial,
    WaterSource,
)
from income.models import (
    CellularNetwork,
    FamilyAccess,
    FamilyAssets,
    FamilyExpenditures,
    FamilyIncome,
    GrossMonthlyIncome,
    InternetProvider,
    PersonalProperty,
    RadioProgram,
)
from nutrition.models import (
    NutritionProfile,
    Sickness,
)

from families import constants


class ProfileSpider(scrapy.Spider):
    name = 'profile'

    address_xpath = '//*[@name="wpcf[fpis-demography-head-of-the-family-address]"]/@value'
    birth_date_xpath = '//*[@name="wpcf[fpis-demography-head-date-of-birth][display-only]"]/@value'
    civil_status_xpath = '//*[@name="wpcf[fpis-demography-head-civil-status]"]//option[@selected]/text()'
    gender_xpath = '//*[@name="wpcf[fpis-demography-head-gender]"]//option[@selected]/text()'
    lgu_xpath = '//*[@name="wpcf[fpis-demography-lgu]"]//option[@selected]/text()'
    series_number_xpath = '//*[@name="wpcf[fpis-demography-series-number]"]/@value'
    tracking_number_xpath = '//*[@name="wpcf[fpis-demography-series-number]"]/@value'
    male_children_xpath = '//*[@name="wpcf[fpis-demography-male-children]"]/@value'
    female_children_xpath = '//*[@name="wpcf[fpis-demography-emale-children]"]/@value'
    head_contact_xpath = '//*[@name="wpcf[fpis-demography-head-contact-number]"]/@value'
    head_cellphone_xpath = '//*[@name="wpcf[fpis-demography-head-cellphone-number]"]/@value'
    head_religion_xpath = '//*[@name="wpcf[fpis-demography-head-religion]"]//option[@selected]/text()'
    head_religion_others_xpath = '//*[@name="wpcf[fpis-demography-other-religion]"]/@value'
    head_blood_type_xpath = '//*[@name="wpcf[fpis-demography-head-blood-type]"]//option[@selected]/text()'
    head_educational_xpath = '//*[@name="wpcf[fpis-demography-head-educational]"]//option[@selected]/text()'
    tribe_type_xpath = '//*[@name="wpcf[fpis-demography-head-type-of-tribe]"]//option[@selected]/text()'
    tribe_xpath = '//*[@name="wpcf[fpis-demography-head-tribe]"]/@value'
    government_aid_received_xpath = '//*[@name="wpcf[fpis-famprofiles-head-aid]"]//option[@selected]/text()'

    spouse_name_xpath = '//*[@name="wpcf[fpis-demography-spouse-name]"]/@value'
    spouse_birth_date_xpath = '//*[@name="wpcf[fpis-demography-spouse-date-of-birth][display-only]"]/@value'
    spouse_contact_xpath = '//*[@name="wpcf[fpis-demography-spouse-contact-number]"]/@value'
    spouse_religion_xpath = '//*[@name="wpcf[fpis-demography-spouse-religion]"]//option[@selected]/text()'
    spouse_blood_type_xpath = '//*[@name="wpcf[fpis-demography-spouse-blood-type]"]//option[@selected]/text()'
    spouse_educational_xpath = '//*[@name="wpcf[fpis-demography-spouse-educational-attainment]"]//option[@selected]/text()'

    children_in_daycare_xpath = '//*[@name="wpcf[fpis-educ-children-in-day-care]"]/@value'
    children_in_kinder_xpath = '//*[@name="wpcf[fpis-educ-children-in-kindergarten]"]/@value'
    has_children_in_elementary_xpath = '//*[@name="wpcf[fpis-educ-children-in-elementary]"]//option[@selected]/text()'
    has_members_able_to_read_and_write_xpath = '//*[@name="wpcf[fpis-famprofiles-read-write]"]//option[@selected]/text()'
    has_members_able_to_vote_xpath = '//*[@name="wpcf[fpis-educ-able-to-vote]"]//option[@selected]/text()'
    has_members_able_to_attend_barangay_assembly_xpath = '//*[@name="wpcf[fpis-educ-meetings]"]//option[@selected]/text()'
    children_not_in_elementary_xpath = '//*[@name="wpcf[fpis-educ-children-not-in-elementary]"]/@value'
    children_not_in_junior_hs_xpath = '//*[@name="wpcf[fpis-educ-not-in-junior-hs]"]/@value'
    children_not_in_senior_hs_xpath = '//*[@name="wpcf[fpis-educ-not-in-senior-hs]"]/@value'
    children_not_in_college_xpath = '//*[@name="wpcf[fpis-educ-not-in-college]"]/@value'
    children_college_graduate_xpath = '//*[@name="wpcf[fpis-educ-children-college-graduate]"]/@value'
    children_college_level_xpath = '//*[@name="wpcf[fpis-educ-children-college-level]"]/@value'
    children_high_school_graduate_xpath = '//*[@name="wpcf[fpis-educ-children-high-school-graduate]"]/@value'
    children_high_school_level_xpath = '//*[@name="wpcf[fpis-famprofiles-children-high-school-level]"]/@value'
    children_elementary_graduate_xpath = '//*[@name="wpcf[fpis-famprofiles-children-elementary-graduate]"]/@value'
    children_elementary_level_xpath = '//*[@name="wpcf[fpis-famprofiles-children-elementary-level]"]/@value'

    has_newborn_estimated_2_5_xpath = '//*[@name="wpcf[fpis-hn-newborn-2p5-kg]"]//option[@selected]/text()'
    num_newborn_2_5_xpath = '//*[@name="wpcf[fpis-hn-number-of-newborn-2p5kg]"]/@value'
    has_newborn_below_2_5_xpath = '//*[@name="wpcf[fpis-hn-newborn-less-2p5-kg]"]//option[@selected]/text()'
    num_newborn_below_2_5_xpath = '//*[@name="wpcf[fpis-hn-newborn-below-2p5-kg]"]/@value'
    has_children_immunized_xpath = '//*[@name="wpcf[fpis-hn-children-immunized]"]//option[@selected]/text()'
    ages_last_immunization_xpath = '//*[@name="wpcf[fpis-hn-ages-on-last-immunitation]"]/@value'
    has_malnourished_children_xpath = '//*[@name="wpcf[fpis-hn-malnourished-children]"]//option[@selected]/text()'
    malnourished_notes_xpath = '//*[@name="wpcf[fpis-hn-malnourished-children-notes]"]/@value'
    has_pregnancy_supplement_xpath = '//*[@name="wpcf[fpis-hn-pregnancy-supplement-provided]"]//option[@selected]/text()'
    pregnancy_supplement_xpath = '//*[@name="wpcf[fpis-hn-pregnancy-supplement]"]/@value'
    has_prenatal_services_xpath = '//*[@name="wpcf[fpis-hn-prenatal-services-available]"]//option[@selected]/text()'
    prenatal_services_xpath = '//*[@name="wpcf[fpis-hn-prenatal-services-notes]"]/@value'
    has_trained_delivery_xpath = '//*[@name="wpcf[fpis-hn-delivery-by-professionals]"]//option[@selected]/text()'
    deliveries_notes_xpath = '//*[@name="wpcf[fpis-hn-deliveries-notes]"]/@value'
    has_death_below_1_year_old_xpath = '//*[@name="wpcf[fpis-hn-death-babies]"]//option[@selected]/text()'
    death_occurence_notes_xpath = '//*[@name="wpcf[fpis-hn-death-occurence-notes]"]/@value'
    children_sicknesses_xpath = '//*[contains(@name, "wpcf[fpis-hn-common-sickness-children]")][@checked="checked"]/@value'
    children_sicknesses_other_xpath = '//*[@name="wpcf[fpis-hn-other-common-sicknesses-children]"]/@value'
    adults_sicknesses_xpath = '//*[contains(@name, "wpcf[fpis-hn-common-sickness-inflicting-adults]")][@checked="checked"]/@value'
    adults_sicknesses_other_xpath = '//*[@name="wpcf[fpis-hn-other-common-sickness-adults]"]/@value'
    philhealth_membership_xpath = '//*[@name="wpcf[fpis-hn-philhealth-membership]"]//option[@selected]/text()'

    skills_name_xpath = '//*[@name="wpcf[fpis-profskills-{}c1-name]"]/@value'
    skills_age_xpath = '//*[@name="wpcf[fpis-profskills-{}c2-age]"]/@value'
    skills_type_xpath = '//*[@name="wpcf[fpis-profskills-{}c3-type-of-skill]"]/@value'
    skills_degree_xpath = '//*[@name="wpcf[fpis-profskills-{}c4-degree]"]/@value'
    skills_acquired_xpath = '//*[@name="wpcf[fpis-profskills-{}c5-skills-acquired-through]"]/@value'
    skills_remarks_xpath = '//*[@name="wpcf[fpis-profskills-{}c6-remarks]"]/@value'

    ownership_status_xpath = '//*[@name="wpcf[fpis-home-ownership]"]//option[@selected]/text()'
    distance_from_brgy_hall_xpath = '//*[@name="wpcf[fpis-home-location]"]/@value'
    gps_xpath = '//*[@name="wpcf[fpis-home-gps]"]/@value'
    home_settings_xpath = '//*[@name="wpcf[fpis-home-setting]"]//option[@selected]/text()'
    vulnerability_and_risks_xpath = '//*[contains(@name, "wpcf[fpis-home-vulnerability-and-risk]")][@checked="checked"]/@value'
    flood_frequency_xpath = '//*[@name="wpcf[fpis-home-flood]"]//option[@selected]/text()'
    earthquake_frequency_xpath = '//*[@name="wpcf[fpis-home-earthquake]"]//option[@selected]/text()'
    landslide_fequency_xpath = '//*[@name="wpcf[fpis-home-landslide]"]//option[@selected]/text()'
    tornado_frequency_xpath = '//*[@name="wpcf[fpis-home-tornado]"]//option[@selected]/text()'
    storm_surge_xpath = '//*[@name="wpcf[fpis-home-storm-surge]"]//option[@selected]/text()'
    vehicular_accident_xpath = '//*[@name="wpcf[fpis-home-vehicular-accident]"]//option[@selected]/text()'
    armed_conflict_xpath = '//*[@name="wpcf[fpis-home-armed-conflict]"]//option[@selected]/text()'
    disease_outbreak_xpath = '//*[@name="wpcf[fpis-home-disease-outbreak]"]//option[@selected]/text()'
    roof_material_xpath = '//*[@name="wpcf[fpis-home-roof-material]"]//option[@selected]/text()'
    walling_material_xpath = '//*[@name="wpcf[fpis-home-walling-material]"]//option[@selected]/text()'
    build_type_xpath = '//*[@name="wpcf[fpis-home-building-type]"]//option[@selected]/text()'
    building_electricity_xpath = '//*[contains(@name, "wpcf[fpis-home-electricity]")][@checked="checked"]/@value'
    electricity_others_xpath = '//*[@name="wpcf[fpis-home-electricity-others]"]/@value'
    water_source_xpath = '//*[contains(@name, "wpcf[fpis-home-water-source]")][@checked="checked"]/@value'
    drainage_xpath = '//*[@name="wpcf[fpis-home-drainage]"]//option[@selected]/text()'
    toilet_xpath = '//*[@name="wpcf[fpis-home-toilet]"]//option[@selected]/text()'
    garbage_disposal_xpath = '//*[@name="wpcf[fpis-home-garbage-disposal]"]//option[@selected]/text()'
    access_road_distance_xpath = '//*[@name="wpcf[fpis-home-distance-to-access-road]"]/@value'
    home_to_farm_distance_xpath = '//*[@name="wpcf[15-distance-from-home-to-production-area-farm]"]/@value'
    farm_to_road_xpath = '//*[@name="wpcf[fpis-distance-from-farm-road]"]/@value'
    product_transportations_xpath = '//*[contains(@name, "wpcf[fpis-home-transportation-of-products]")][@checked="checked"]/@value'

    farming_xpath = '//*[@name="wpcf[fpis-income-from-farming]"]/@value'
    fishing_xpath = '//*[@name="wpcf[fpis-income-from-fishing]"]/@value'
    government_employment_xpath = '//*[@name="wpcf[fpis-income-government-employment]"]/@value'
    private_employment_xpath = '//*[@name="wpcf[fpis-income-private-employment]"]/@value'
    ofw_remittances_xpath = '//*[@name="wpcf[fpis-income-ofw-remittances]"]/@value'
    pension_xpath = '//*[@name="wpcf[fpis-income-pension]"]/@value'
    subsidies_xpath = '//*[@name="wpcf[fpis-income-4ps-subsidies]"]/@value'
    self_employment_xpath = '//*[@name="wpcf[fpis-income-self-employment]"]/@value'
    business_xpath = '//*[@name="wpcf[fpis-income-from-business]"]/@value'
    other_sources_xpath = '//*[@name="wpcf[fpis-income-other-sources]"]/@value'
    other_income_sources_xpath = '//*[@name="wpcf[fpis-income-other-income-sources]"]/@value'
    gross_monthly_income_xpath = '//*[@name="wpcf[fpis-income-gross-monthly-income]"]//option[@selected]/text()'

    food_xpath = '//*[@name="wpcf[fpis-income-food-expenditures]"]/@value'
    education_xpath = '//*[@name="wpcf[fpis-income-expenditures-for-education]"]/@value'
    medicine_xpath = '//*[@name="wpcf[fpis-income-expenditure-medicine]"]/@value'
    power_bill_xpath = '//*[@name="wpcf[fpis-income-expenditure-power-bill]"]/@value'
    water_bill_xpath = '//*[@name="wpcf[fpis-income-expenditure-water-bill]"]/@value'
    philhealth_xpath = '//*[@name="wpcf[fpis-income-expenditure-philhealth]"]/@value'
    sss_xpath = '//*[@name="wpcf[fpis-income-expenditure-sss]"]/@value'
    pag_ibig_xpath = '//*[@name="wpcf[fpis-income-expenditure-pag-ibig]"]/@value'
    gsis_xpath = '//*[@name="wpcf[fpis-income-expenditure-gsis]"]/@value'
    clothing_xpath = '//*[@name="wpcf[fpis-income-expenditure-clothing]"]/@value'
    insurance_xpath = '//*[@name="wpcf[fpis-income-expenditurer-insurance]"]/@value'
    vehicle_xpath = '//*[@name="wpcf[fpis-income-expenditure-vehicle]"]/@value'
    taxes_xpath = '//*[@name="wpcf[fpis-income-expenditure-taxes]"]/@value'
    debt_repayments_xpath = '//*[@name="wpcf[fpis-income-expenditure-debt-repayments]"]/@value'
    house_rental_xpath = '//*[@name="wpcf[fpis-income-expenditure-house-rental]"]/@value'
    house_helper_xpath = '//*[@name="wpcf[fpis-income-expenditure-house-helper]"]/@value'
    vices_xpath = '//*[@name="wpcf[fpis-income-expenditure-vices]"]/@value'
    others_xpath = '//*[@name="wpcf[fpis-income-expenditure-other]"]/@value'
    other_expenditures_xpath = '//*[@name="wpcf[fpis-income-expenditure-specify]"]/@value'

    monthly_bank_savings_xpath = '//*[@name="wpcf[fpis-income-savings-bank]"]/@value'
    monthly_cooperative_savings_xpath = '//*[@name="wpcf[fpis-income-savings-cooperative]"]/@value'
    monthly_others_savings_xpath = '//*[@name="wpcf[fpis-income-savings-others]"]/@value'
    personal_properties_xpath = '//*[contains(@name, "wpcf[fpis-income-personal-properties]")][@checked="checked"]/@value'

    radio_programs_xpath = '//*[contains(@name, "wpcf[fpis-income-communications-radio]")][@checked="checked"]/@value'
    cellular_networks_xpath = '//*[contains(@name, "wpcf[fpis-income-communications-cellular-network]")][@checked="checked"]/@value'
    internet_providers_xpath = '//*[contains(@name, "wpcf[fpis-income-communications-internet-provider]")][@checked="checked"]/@value'
    local_newspaper_xpath = '//*[@name="wpcf[fpis-income-local-newspaper]"]/@value'
    national_newspaper_xpath = '//*[@name="wpcf[fpis-income-national-newspaper]"]/@value'
    other_access_xpath = '//*[@name="wpcf[fpis-income-communications-others]"]/@value'
    access_frequency_xpath = '//*[@name="wpcf[fpis-income-communication-frequency]"]/@value'

    total_area_xpath = '//*[@name="wpcf[fpsi-agri-farm-area]"]/@value'
    other_areas_xpath = '//*[@name="wpcf[fpsi-agri-farms-other-areas]"]/@value'
    owned_xpath = '//*[@name="wpcf[fpsi-agri-farm-owned]"]/@value'
    rented_xpath = '//*[@name="wpcf[fpsi-agri-farm-rented]"]/@value'
    tenant_xpath = '//*[@name="wpcf[fpsi-agri-farm-tenant]"]/@value'
    mortgaged_xpath = '//*[@name="wpcf[fpsi-agri-farm-mortgaged]"]/@value'
    martgaged_acquired_xpath = '//*[@name="wpcf[fpsi-agri-mortgage-acquired]"]/@value'
    tenurial_instrument_xpath = '//*[@name="wpcf[fpis-agri-tenurial-instrument]"]//option[@selected]/text()'
    total_land_area_xpath = '//*[@name="wpcf[fpis-agri-total-land-area]"]/@value'
    total_land_area_others_xpath = '//*[@name="wpcf[fpis-agri-total-land-area-others]"]/@value'
    farming_practice_xpath = '//*[@name="wpcf[fpis-agri-farming-practice]"]//option[@selected]/text()'
    farming_practice_others_xpath = '//*[@name="wpcf[fpis-agri-farming-practice-others]"]/@value'
    workers_xpath = '//*[contains(@name, "wpcf[fpis-agri-working-at-the-farm]")][@checked="checked"]/@value'
    other_workers_xpath = '//*[@name="wpcf[fpis-agri-others-working-at-the-farm]"]/@value'
    waste_practices_xpath = '//*[contains(@name, "wpcf[fpis-agri-waste-recycling]")][@checked="checked"]/@value'
    other_waste_practices_xpath = '//*[@name="wpcf[fpis-agri-other-waste-recycling]"]/@value'
    pakyaw_or_labor_xpath = '//*[@name="wpcf[fpis-agri-pakyaw-labor]"]//option[@selected]/text()'
    pakyaw_value_xpath = '//*[@name="wpcf[fpis-agri-pakyaw-yes]"]/@value'
    labor_type_xpath = '//*[@name="wpcf[fpis-agri-{}c1-labor]"]/@value'
    labor_rate_xpath = '//*[@name="wpcf[fpis-agri-{}c2-rate]"]/@value'

    def start_requests(self):
        return [
            scrapy.FormRequest(
                "http://malita.localhost/wordpress/wp-login.php",
                formdata={'log': 'tsadmin', 'pwd': 'admin'},
                callback=self.start_crawl
            ),
        ]

    def start_crawl(self, response):
        db_num = '33'
        url = 'http://malita.localhost/wordpress/wp-admin/post.php?post={}&action=edit'
        with open(os.path.join(settings.BASE_DIR, 'fixes', '{}a.txt'.format(db_num))) as file_:
            db = '{}a'.format(db_num)
            for id in file_:
                id = id.strip()
                yield scrapy.Request(url=url.format(id), callback=self.parse, meta={'id': id, 'db': db})

    def _clean_gps_point(self, gps_point):
        if gps_point is None:
            return None

        if gps_point.split('.') == 2 and len(gps_point.split('.')[0]) == 0:
            return None
        elif gps_point.split('.') != 2:
            return None

        try:
            return decimal.Decimal(gps_point)
        except (decimal.InvalidOperation, decimal.InvalidContext):
            return None

    def _get_foreign_key_value(self, response, xpath, model):
        value = self._get_value(response, xpath)
        if value.find('not set') == -1:
            try:
                instance, _ = model.objects.get_or_create(name=value.strip())
            except FieldError:
                instance, _ = model.objects.get_or_create(value=value.strip())
            return instance
        return None

    def _get_choice_value(self, response, xpath, choices):
        value = response.xpath(xpath).extract_first() or ''
        for choice in choices:
            if value.lower().strip() == choice[1].lower():
                return choice[0]
        return ''

    def _get_bool_value(self, response, xpath):
        value = response.xpath(xpath).extract_first() or ''
        value = value.strip().lower()
        yes_choices = [
            'yes',
            'immunized',
            'active',
        ]
        no_choices = [
            'no',
            'not immunized',
            'inactive',
        ]
        if value in yes_choices:
            return True
        elif value in no_choices:
            return False
        else:
            return None

    def _convert_to_float(self, frac_str):
        num, denom = frac_str.split('/')
        try:
            leading, num = num.split(' ')
            whole = float(leading)
        except ValueError:
            whole = 0
        frac = float(num) / float(denom)
        return whole - frac if whole < 0 else whole + frac

    def _get_number_value(self, response, xpath):
        value = response.xpath(xpath).extract_first()

        if value:
            value = value.replace(',', '')
            if '/' in value:
                return self._convert_to_float(value)
            return float(value)

        return None

    def _get_checkbox_values(self, response, xpath):
        return [val.strip() for val in response.xpath(xpath).extract()]

    def _get_value(self, response, xpath):
        value = response.xpath(xpath).extract_first() or ''
        return value.strip() if value else value

    @transaction.atomic
    def parse(self, response):
        lgu, _ = LGU.objects.get_or_create(
            name=self._get_value(response, self.lgu_xpath)
        )

        try:
            birth_date = parser.parse(
                self._get_value(response, self.birth_date_xpath)
            ).date()
        except Exception:
            birth_date = None
            print(response.url)

        name = response.css('#title::attr(value)').extract_first().strip()
        wordpress_id = response.meta.get('id')
        wordpress_db = response.meta.get('db')

        if Family.objects.filter(wordpress_id=wordpress_id, wordpress_db=wordpress_db, head__name=name).exists():
            print('Already exists: {} - {}'.format(wordpress_id, name))
            return

        head = Head.objects.create(
            name=name,
            gender=self._get_choice_value(response, self.gender_xpath, constants.GENDER_CHOICES),
            civil_status=self._get_choice_value(response, self.civil_status_xpath, constants.CIVIL_STATUS_CHOICES),
            birth_date=birth_date,
            contact_number=self._get_value(response, self.head_contact_xpath),
            cellphone_number=self._get_value(response, self.head_cellphone_xpath),
            religion=self._get_value(response, self.head_religion_xpath),
            religion_others=self._get_value(response, self.head_religion_others_xpath),
            blood_type=self._get_choice_value(
                response, self.head_blood_type_xpath, constants.BLOOD_TYPE_CHOICES
            ),
            highest_educational_attainment=self._get_choice_value(
                response, self.head_educational_xpath, constants.EDUCATIONAL_ATTAINMENT_CHOICES
            ),

        )

        try:
            birth_date = parser.parse(
                self._get_value(response, self.spouse_birth_date_xpath)
            ).date()
        except Exception:
            birth_date = None
            print(response.url)

        spouse_name = self._get_value(response, self.spouse_name_xpath)
        if spouse_name:
            spouse = Spouse.objects.create(
                name=spouse_name,
                birth_date=birth_date,
                contact_number=self._get_value(response, self.spouse_contact_xpath),
                religion=self._get_value(response, self.spouse_religion_xpath),
                blood_type=self._get_choice_value(
                    response, self.spouse_blood_type_xpath, constants.BLOOD_TYPE_CHOICES
                ),
                highest_educational_attainment=self._get_choice_value(
                    response, self.spouse_educational_xpath, constants.EDUCATIONAL_ATTAINMENT_CHOICES
                ),
            )
        else:
            spouse = None

        dependents_xpath = '//*[contains(@name, "wpcf[fpis-demography-dependents]")]/text()'
        family = Family.objects.create(
            wordpress_db=wordpress_db,
            wordpress_id=wordpress_id,
            head=head,
            spouse=spouse,
            lgu=lgu,
            tracking_number=self._get_value(response, self.tracking_number_xpath),
            series_number=self._get_value(response, self.series_number_xpath),
            address=self._get_value(response, self.address_xpath),
            num_of_male_children=self._get_number_value(response, self.male_children_xpath),
            num_of_female_children=self._get_number_value(response, self.female_children_xpath),
            tribe_type=self._get_choice_value(
                response, self.tribe_type_xpath, Family.TRIBE_TYPE_CHOICES
            ),
            tribe_name=self._get_value(response, self.tribe_xpath),
            government_aid_received=self._get_choice_value(
                response, self.government_aid_received_xpath, Family.AID_RECEIVED_CHOICES
            ),
            dependents_text=self._get_value(response, dependents_xpath)
        )

        EducationalProfile.objects.create(
            family=family,
            children_in_daycare=self._get_number_value(response, self.children_in_daycare_xpath),
            children_in_kinder=self._get_number_value(response, self.children_in_kinder_xpath),
            has_children_in_elementary=self._get_bool_value(
                response, self.has_children_in_elementary_xpath
            ),
            has_members_able_to_read_and_write=self._get_bool_value(
                response, self.has_members_able_to_read_and_write_xpath
            ),
            has_members_able_to_vote=self._get_bool_value(
                response, self.has_members_able_to_vote_xpath
            ),
            has_members_able_to_attend_barangay_assembly=self._get_bool_value(
                response, self.has_members_able_to_attend_barangay_assembly_xpath
            ),
            children_not_in_elementary=self._get_number_value(response, self.children_not_in_elementary_xpath),
            children_not_in_junior_hs=self._get_number_value(response, self.children_not_in_junior_hs_xpath),
            children_not_in_senior_hs=self._get_number_value(response, self.children_not_in_senior_hs_xpath),
            children_not_in_college=self._get_number_value(response, self.children_not_in_college_xpath),
            children_college_graduate=self._get_number_value(response, self.children_college_graduate_xpath),
            children_college_level=self._get_number_value(response, self.children_college_level_xpath),
            children_high_school_graduate=self._get_number_value(response, self.children_high_school_graduate_xpath),
            children_high_school_level=self._get_number_value(response, self.children_high_school_level_xpath),
            children_elementary_graduate=self._get_number_value(response, self.children_elementary_graduate_xpath),
            children_elementary_level=self._get_number_value(response, self.children_elementary_level_xpath),
        )


        try:
            num_newborn_2_5 = int(self._get_number_value(response, self.num_newborn_2_5_xpath))
        except (TypeError, ValueError):
            num_newborn_2_5 = None

        try:
            num_newborn_below_2_5 = int(self._get_number_value(response, self.num_newborn_below_2_5_xpath))
        except (TypeError, ValueError):
            num_newborn_below_2_5 = None

        nutrition_profile = NutritionProfile.objects.create(
            family=family,
            has_newborn_estimated_2_5=self._get_bool_value(
                response, self.has_newborn_estimated_2_5_xpath
            ),
            num_newborn_2_5=num_newborn_2_5,
            has_newborn_below_2_5=self._get_bool_value(
                response, self.has_newborn_below_2_5_xpath
            ),
            num_newborn_below_2_5=num_newborn_below_2_5,
            has_children_immunized=self._get_bool_value(
                response, self.has_children_immunized_xpath
            ),
            ages_last_immunization=self._get_value(response, self.ages_last_immunization_xpath),
            has_malnourished_children=self._get_bool_value(
                response, self.has_malnourished_children_xpath
            ),
            malnourished_notes=self._get_value(response, self.malnourished_notes_xpath),
            has_pregnancy_supplement=self._get_bool_value(
                response, self.has_pregnancy_supplement_xpath
            ),
            pregnancy_supplement=self._get_value(response, self.pregnancy_supplement_xpath),
            has_prenatal_services=self._get_bool_value(
                response, self.has_prenatal_services_xpath
            ),
            prenatal_services=self._get_value(response, self.prenatal_services_xpath),
            has_trained_delivery=self._get_bool_value(
                response, self.has_trained_delivery_xpath
            ),
            deliveries_notes=self._get_value(response, self.deliveries_notes_xpath),
            has_death_below_1_year_old=self._get_bool_value(
                response, self.has_death_below_1_year_old_xpath
            ),
            death_occurence_notes=self._get_value(response, self.death_occurence_notes_xpath),
            children_sicknesses_other=self._get_value(response, self.children_sicknesses_other_xpath),
            adults_sicknesses_other=self._get_value(response, self.adults_sicknesses_other_xpath),
            philhealth_membership=self._get_choice_value(
                response, self.philhealth_membership_xpath, NutritionProfile.PHILHEALTH_CHOICES
            ),
        )

        for sickness_name in self._get_checkbox_values(response, self.children_sicknesses_xpath):
            sickness, _ = Sickness.objects.get_or_create(name=sickness_name)
            nutrition_profile.children_sicknesses.add(sickness)

        for sickness_name in self._get_checkbox_values(response, self.adults_sicknesses_xpath):
            sickness, _ = Sickness.objects.get_or_create(name=sickness_name)
            nutrition_profile.adults_sicknesses.add(sickness)

        for i in range(1, 6):
            name = self._get_value(response, self.skills_name_xpath.format(i))
            age = self._get_value(response, self.skills_age_xpath.format(i))
            type_ = self._get_value(response, self.skills_type_xpath.format(i))
            degree = self._get_value(response, self.skills_degree_xpath.format(i))
            acquired = self._get_value(response, self.skills_acquired_xpath.format(i))
            remarks = self._get_value(response, self.skills_remarks_xpath.format(i))

            if any([name, age, type_, degree, acquired, remarks]):
                FamilyProfession.objects.create(
                    family=family,
                    name=name,
                    age=age,
                    type_of_skill=type_,
                    degree=degree,
                    skills_acquired_through=acquired,
                    remarks=remarks,
                )

        gps_reading = self._get_value(response, self.gps_xpath)
        if len(gps_reading.split('/')) == 2:
            gps_lat, gps_lon = gps_reading.split('/')
        elif len(gps_reading.split('.')) == 2:
            half_idx = gps_reading.index('.') + 7
            gps_lat = gps_reading[:half_idx]
            gps_lon = gps_reading[half_idx:]
        else:
            gps_lat = None
            gps_lon = None

        gps_lat = self._clean_gps_point(gps_lat)
        gps_lon = self._clean_gps_point(gps_lon)

        housing = FamilyHousing.objects.create(
            family=family,
            ownership_status=self._get_choice_value(
                response, self.ownership_status_xpath, FamilyHousing.OWNERSHIP_CHOICES
            ),
            distance_from_brgy_hall=self._get_value(response, self.distance_from_brgy_hall_xpath),
            gps_lat=gps_lat,
            gps_lon=gps_lon,
            home_settings=self._get_choice_value(
                response, self.home_settings_xpath, FamilyHousing.HOME_CHOICES
            ),
            flood_frequency=self._get_choice_value(
                response, self.flood_frequency_xpath, FamilyHousing.FREQ_CHOICES
            ),
            earthquake_frequency=self._get_choice_value(
                response, self.earthquake_frequency_xpath, FamilyHousing.FREQ_CHOICES
            ),
            landslide_frequency=self._get_choice_value(
                response, self.landslide_fequency_xpath, FamilyHousing.FREQ_CHOICES
            ),
            tornado_frequency=self._get_choice_value(
                response, self.tornado_frequency_xpath, FamilyHousing.FREQ_CHOICES
            ),
            storm_surge=self._get_choice_value(
                response, self.storm_surge_xpath, FamilyHousing.FREQ_CHOICES
            ),
            vehicular_accident=self._get_choice_value(
                response, self.vehicular_accident_xpath, FamilyHousing.FREQ_CHOICES
            ),
            armed_conflict=self._get_choice_value(
                response, self.armed_conflict_xpath, FamilyHousing.FREQ_CHOICES
            ),
            disease_outbreak=self._get_choice_value(
                response, self.disease_outbreak_xpath, FamilyHousing.FREQ_CHOICES
            ),
            roof_material=self._get_foreign_key_value(response, self.roof_material_xpath, RoofMaterial),
            walling_material=self._get_foreign_key_value(response, self.walling_material_xpath, WallingMaterial),
            building_type=self._get_foreign_key_value(response, self.build_type_xpath, BuildingType),
            electricity_others=self._get_value(response, self.electricity_others_xpath),
            drainage=self._get_foreign_key_value(response, self.drainage_xpath, DrainageType),
            toilet=self._get_foreign_key_value(response, self.toilet_xpath, ToiletType),
            garbage_disposal=self._get_foreign_key_value(response, self.garbage_disposal_xpath, GarbageDisposal),
            access_road_distance=self._get_value(response, self.access_road_distance_xpath),
            home_to_farm_distance=self._get_value(response, self.home_to_farm_distance_xpath),
            farm_to_road=self._get_value(response, self.farm_to_road_xpath),
        )

        for name in self._get_checkbox_values(response, self.vulnerability_and_risks_xpath):
            instance, _ = HouseLocationRisk.objects.get_or_create(name=name)
            housing.vulnerability_and_risks.add(instance)

        for name in self._get_checkbox_values(response, self.building_electricity_xpath):
            instance, _ = BuildingElectricity.objects.get_or_create(name=name)
            housing.building_electricity.add(instance)

        for name in self._get_checkbox_values(response, self.water_source_xpath):
            instance, _ = WaterSource.objects.get_or_create(name=name)
            housing.water_source.add(instance)

        for name in self._get_checkbox_values(response, self.product_transportations_xpath):
            instance, _ = ProductTransportation.objects.get_or_create(name=name)
            housing.product_transportations.add(instance)

        FamilyIncome.objects.create(
            family=family,
            farming=self._get_number_value(response, self.farming_xpath),
            fishing=self._get_number_value(response, self.fishing_xpath),
            government_employment=self._get_number_value(response, self.government_employment_xpath),
            private_employment=self._get_number_value(response, self.private_employment_xpath),
            ofw_remittances=self._get_number_value(response, self.ofw_remittances_xpath),
            pension=self._get_number_value(response, self.pension_xpath),
            subsidies=self._get_number_value(response, self.subsidies_xpath),
            self_employment=self._get_number_value(response, self.self_employment_xpath),
            business=self._get_number_value(response, self.business_xpath),
            other_sources=self._get_number_value(response, self.other_sources_xpath),
            other_income_sources=self._get_value(response, self.other_income_sources_xpath),
            gross_monthly_income=self._get_foreign_key_value(
                response, self.gross_monthly_income_xpath, GrossMonthlyIncome
            )
        )

        FamilyExpenditures.objects.create(
            family=family,
            food=self._get_number_value(response, self.food_xpath),
            education=self._get_number_value(response, self.education_xpath),
            medicine=self._get_number_value(response, self.medicine_xpath),
            power_bill=self._get_number_value(response, self.power_bill_xpath),
            water_bill=self._get_number_value(response, self.water_bill_xpath),
            philhealth=self._get_number_value(response, self.philhealth_xpath),
            sss=self._get_number_value(response, self.sss_xpath),
            pag_ibig=self._get_number_value(response, self.pag_ibig_xpath),
            gsis=self._get_number_value(response, self.gsis_xpath),
            clothing=self._get_number_value(response, self.clothing_xpath),
            insurance=self._get_number_value(response, self.insurance_xpath),
            vehicle=self._get_number_value(response, self.vehicle_xpath),
            taxes=self._get_number_value(response, self.taxes_xpath),
            debt_repayments=self._get_number_value(response, self.debt_repayments_xpath),
            house_rental=self._get_number_value(response, self.house_rental_xpath),
            house_helper=self._get_number_value(response, self.house_helper_xpath),
            vices=self._get_number_value(response, self.vices_xpath),
            others=self._get_number_value(response, self.others_xpath),
            other_expenditures=self._get_value(response, self.other_expenditures_xpath),
        )

        assets = FamilyAssets.objects.create(
            family=family,
            monthly_bank_savings=self._get_number_value(response, self.monthly_bank_savings_xpath),
            monthly_cooperative_savings=self._get_number_value(response, self.monthly_cooperative_savings_xpath),
            monthly_others_savings=self._get_number_value(response, self.monthly_others_savings_xpath),
        )

        for name in self._get_checkbox_values(response, self.personal_properties_xpath):
            instance, _ = PersonalProperty.objects.get_or_create(name=name)
            assets.personal_properties.add(instance)

        family_access = FamilyAccess.objects.create(
            family=family,
            local_newspaper=self._get_value(response, self.local_newspaper_xpath),
            national_newspaper=self._get_value(response, self.national_newspaper_xpath),
            other_access=self._get_value(response, self.other_access_xpath),
            access_frequency=self._get_value(response, self.access_frequency_xpath),
        )

        for name in self._get_checkbox_values(response, self.radio_programs_xpath):
            instance, _ = RadioProgram.objects.get_or_create(name=name)
            family_access.radio_programs.add(instance)

        for name in self._get_checkbox_values(response, self.cellular_networks_xpath):
            instance, _ = CellularNetwork.objects.get_or_create(name=name)
            family_access.cellular_networks.add(instance)

        for name in self._get_checkbox_values(response, self.internet_providers_xpath):
            instance, _ = InternetProvider.objects.get_or_create(name=name)
            family_access.internet_providers.add(instance)

        family_farm = FamilyFarm.objects.create(
            family=family,
            total_area=self._get_value(response, self.total_area_xpath),
            other_areas=self._get_value(response, self.other_areas_xpath),
            owned=self._get_value(response, self.owned_xpath),
            rented=self._get_value(response, self.rented_xpath),
            tenant=self._get_value(response, self.tenant_xpath),
            mortgaged=self._get_value(response, self.mortgaged_xpath),
            martgaged_acquired=self._get_value(response, self.martgaged_acquired_xpath),
            tenurial_instrument=self._get_foreign_key_value(
                response, self.tenurial_instrument_xpath, TenurialInstrument
            ),
            total_land_area=self._get_value(response, self.total_land_area_xpath),
            total_land_area_others=self._get_value(response, self.total_land_area_others_xpath),
            farming_practice=self._get_foreign_key_value(
                response, self.farming_practice_xpath, FarmingPractice
            ),
            farming_practice_others=self._get_value(response, self.farming_practice_others_xpath),
            other_workers=self._get_value(response, self.other_workers_xpath),
            other_waste_practices=self._get_value(response, self.other_waste_practices_xpath),
            pakyaw_or_labor=self._get_choice_value(
                response, self.pakyaw_or_labor_xpath, constants.YES_NO_CHOICES
            ),
            pakyaw_value=self._get_value(response, self.pakyaw_value_xpath),
        )

        for name in self._get_checkbox_values(response, self.workers_xpath):
            instance, _ = FarmWorker.objects.get_or_create(name=name)
            family_farm.workers.add(instance)

        for name in self._get_checkbox_values(response, self.waste_practices_xpath):
            instance, _ = FarmWastePractice.objects.get_or_create(name=name)
            family_farm.waste_practices.add(instance)

        for i in range(10, 13):
            type_ = self._get_value(response, self.labor_type_xpath.format(i))
            rate_per_hour = self._get_value(response, self.labor_rate_xpath.format(i))
            if any([type_, rate_per_hour]):
                FarmLabor.objects.create(
                    family_farm=family_farm,
                    type=type_,
                    rate_per_hour=rate_per_hour,
                )

        farm_type_xpath = '//*[@name="wpcf[fpis-agri-rice-rainfed-irrigated]"]//option[@selected]/text()'
        irrigated_value_xpath = '//*[@name="wpcf[fpis-agri-rice-irrigated-details]"]/@value'
        inorganic_bags_xpath = '//*[@name="wpcf[fpis-agri-rice-bags-inorganic-fertilizers]"]/@value'
        organic_bags_xpath = '//*[@name="wpcf[fpis-agri-rice-bags-organic-fertilizers]"]/@value'
        source_bags_xpath = '//*[@name="wpcf[fpis-agri-rice-fertilizers-source]"]/@value'
        marketing_xpath = '//*[contains(@name, "wpcf[fpis-agri-rice-marketing-farm-produce]")][@checked="checked"]/@value'
        market_outlets_xpath = '//*[contains(@name, "wpcf[fpis-agri-rice-market-outlet]")][@checked="checked"]/@value'
        applies_microbial_xpath = '//*[@name="wpcf[fpis-agri-rice-apply-microbial]"]//option[@selected]/text()'
        microbial_reason_xpath = '//*[@name="wpcf[fpis-agri-rice-why-microbial]"]/@value'
        financing_practices_xpath = '//*[contains(@name, "wpcf[fpis-agri-rice-financing]")][@checked="checked"]/@value'

        crop_type, _ = CropType.objects.get_or_create(name='Staple Crop (Rice)')
        family_crop_info = FamilyCropInfo.objects.create(
            family=family,
            crop_type=crop_type,
            farm_type=self._get_choice_value(
                response, farm_type_xpath, FARM_TYPE_CHOICES
            ),
            irrigated_value=self._get_value(response, irrigated_value_xpath),
            inorganic_bags=self._get_value(response, inorganic_bags_xpath),
            organic_bags=self._get_value(response, organic_bags_xpath),
            source_bags=self._get_value(response, source_bags_xpath),
            applies_microbial=self._get_choice_value(
                response, applies_microbial_xpath, constants.YES_NO_CHOICES
            ),
            microbial_reason=self._get_value(response, microbial_reason_xpath),
        )

        for name in self._get_checkbox_values(response, marketing_xpath):
            instance, _ = FarmMarketing.objects.get_or_create(name=name)
            family_crop_info.marketing.add(instance)

        for name in self._get_checkbox_values(response, market_outlets_xpath):
            instance, _ = MarketOutlet.objects.get_or_create(name=name)
            family_crop_info.market_outlets.add(instance)

        for name in self._get_checkbox_values(response, financing_practices_xpath):
            instance, _ = FinancingPractice.objects.get_or_create(name=name)
            family_crop_info.financing_practices.add(instance)

        crop_names = [
            'Lowland/Organic rice',
            'Lowland/Inbred rice',
            'Upland/Organic rice',
            'Hybrid rice/Irrigated',
        ]

        hectares_xpath = '//*[@name="wpcf[fpis-agri-rice-{}c1-hectares]"]/@value'
        kilos_xpath = '//*[@name="wpcf[fpis-agri-rice-{}c2-tons-per-cropping]"]/@value'
        income_xpath = '//*[@name="wpcf[fpis-agri-rice-{}c3-income-per-cropping]"]/@value'
        costs_xpath = '//*[@name="wpcf[fpis-agri-rice-{}c4-costs-per-cropping]"]/@value'

        for i in range(10, 14):
            hectares = self._get_number_value(response, hectares_xpath.format(i))
            kilos = self._get_number_value(response, kilos_xpath.format(i))
            income = self._get_number_value(response, income_xpath.format(i))
            costs = self._get_number_value(response, costs_xpath.format(i))
            crop_name = crop_names.pop()
            crop, _ = Crop.objects.get_or_create(name=crop_name)

            if any([hectares, kilos, income, costs]):
                FamilyCrop.objects.create(
                    info=family_crop_info,
                    crop=crop,
                    hectares=hectares,
                    kilos_per_cropping=kilos,
                    income_per_cropping=income,
                    production_costs_per_cropping=costs,
                )

        inorganic_bags_xpath = '//*[@name="wpcf[fpis-agri-corn-bags-inorganic-fertilizers]"]/@value'
        organic_bags_xpath = '//*[@name="wpcf[fpis-agri-corn-bags-organic-fertilizers]"]/@value'
        source_bags_xpath = '//*[@name="wpcf[fpis-agri-corn-fertilizers-source]"]/@value'
        seed_types_xpath = '//*[contains(@name, "wpcf[fpis-agri-corn-19a-seeds]")][@checked="checked"]/@value'
        other_seed_types_xpath = '//*[@name="wpcf[fpis-agri-corn-19b-other-seeds]"]/@value'

        marketing_xpath = '//*[contains(@name, "wpcf[fpis-agri-corn-marketing-farm-produce]")][@checked="checked"]/@value'
        market_outlets_xpath = '//*[contains(@name, "wpcf[fpis-agri-corn-market-outlet]")][@checked="checked"]/@value'
        applies_microbial_xpath = '//*[@name="wpcf[fpis-agri-corn-apply-microbial]"]//option[@selected]/text()'
        financing_practices_xpath = '//*[contains(@name, "wpcf[fpis-agri-corn-financing]")][@checked="checked"]/@value'

        crop_type, _ = CropType.objects.get_or_create(name='Staple Crop (Corn)')
        family_crop_info = FamilyCropInfo.objects.create(
            family=family,
            crop_type=crop_type,
            inorganic_bags=self._get_value(response, inorganic_bags_xpath),
            organic_bags=self._get_value(response, organic_bags_xpath),
            source_bags=self._get_value(response, source_bags_xpath),
            other_seed_types=self._get_value(response, other_seed_types_xpath),
        )

        for name in self._get_checkbox_values(response, seed_types_xpath):
            instance, _ = SeedType.objects.get_or_create(name=name)
            family_crop_info.seed_types.add(instance)

        for name in self._get_checkbox_values(response, marketing_xpath):
            instance, _ = FarmMarketing.objects.get_or_create(name=name)
            family_crop_info.marketing.add(instance)

        for name in self._get_checkbox_values(response, market_outlets_xpath):
            instance, _ = MarketOutlet.objects.get_or_create(name=name)
            family_crop_info.market_outlets.add(instance)

        for name in self._get_checkbox_values(response, financing_practices_xpath):
            instance, _ = FinancingPractice.objects.get_or_create(name=name)
            family_crop_info.financing_practices.add(instance)

        crop_names = [
            'Others ADLAY',
            'White Corn',
            'Yellow Corn',
        ]

        hectares_xpath = '//*[@name="wpcf[fpis-agri-corn-{}c1-hectares]"]/@value'
        kilos_xpath = '//*[@name="wpcf[fpis-agri-corn-{}c2-tons-per-cropping]"]/@value'
        income_xpath = '//*[@name="wpcf[fpis-agri-corn-{}c3-income-per-cropping]"]/@value'
        costs_xpath = '//*[@name="wpcf[fpis-agri-corn-{}c4-costs-per-cropping]"]/@value'

        for i in range(15, 18):
            hectares = self._get_number_value(response, hectares_xpath.format(i))
            kilos = self._get_number_value(response, kilos_xpath.format(i))
            income = self._get_number_value(response, income_xpath.format(i))
            costs = self._get_number_value(response, costs_xpath.format(i))
            crop_name = crop_names.pop()
            crop, _ = Crop.objects.get_or_create(name=crop_name)

            if any([hectares, kilos, income, costs]):
                FamilyCrop.objects.create(
                    info=family_crop_info,
                    crop=crop,
                    hectares=hectares,
                    kilos_per_cropping=kilos,
                    income_per_cropping=income,
                    production_costs_per_cropping=costs,
                )


        #==================== START =========================#
        marketing_xpath = '//*[contains(@name, "wpcf[fpis-agri-hvc-14-marketing]")][@checked="checked"]/@value'
        market_outlets_xpath = '//*[contains(@name, "wpcf[fpis-agri-hvc-15a-marketing-outlet]")][@checked="checked"]/@value'
        other_market_outlets_xpath = '//*[@name="wpcf[fpis-agri-hvc-15b-other-marketing-outlets]"]/@value'

        crop_type, _ = CropType.objects.get_or_create(name='High Value Crops')
        family_crop_info = FamilyCropInfo.objects.create(
            family=family,
            crop_type=crop_type,
            other_market_outlets=self._get_value(response, other_market_outlets_xpath),
        )

        for name in self._get_checkbox_values(response, marketing_xpath):
            instance, _ = FarmMarketing.objects.get_or_create(name=name)
            family_crop_info.marketing.add(instance)

        for name in self._get_checkbox_values(response, market_outlets_xpath):
            instance, _ = MarketOutlet.objects.get_or_create(name=name)
            family_crop_info.market_outlets.add(instance)

        crop_names = [
            'Pomelo / Citrus',
            'Coconut',
            'Durian',
            'Rambutan',
            'Mango',
            'Lanzones',
            'Coffee',
            'Cacao',
            'Abaca',
            'Cavendish',
            'Latundan',
            'Lacatan',
            'Banana',
        ]

        hills_xpath = '//*[@name="wpcf[fpis-agri-hvc-{}c1-hills]"]/@value'
        hectares_xpath = '//*[@name="wpcf[fpis-agri-hvc-{}c2-hectares]"]/@value'
        income_xpath = '//*[@name="wpcf[fpis-agri-hvc-{}c3-income-per-cropping]"]/@value'
        costs_xpath = '//*[contains(@name, "wpcf[fpis-agri-hvc-{}c4")]/@value'

        for i in range(1, 14):
            hills = self._get_number_value(response, hills_xpath.format(i))
            hectares = self._get_number_value(response, hectares_xpath.format(i))
            income = self._get_number_value(response, income_xpath.format(i))
            costs = self._get_number_value(response, costs_xpath.format(i))
            crop_name = crop_names.pop()
            crop, _ = Crop.objects.get_or_create(name=crop_name)

            if any([hectares, hills, income, costs]):
                FamilyCrop.objects.create(
                    info=family_crop_info,
                    crop=crop,
                    hectares=hectares,
                    hills=hills,
                    income_per_cropping=income,
                    production_costs_per_cropping=costs,
                )

        # =============== Start ================
        vegetables_xpath = '//*[contains(@name, "wpcf[fpis-agri-vrc-backyard]")][@checked="checked"]/@value'
        other_vegetables_xpath = '//*[@name="wpcf[fpis-agri-vrc-1b-others]"]/@value'

        crop_type, _ = CropType.objects.get_or_create(name='Vegetable and Root Crops')
        family_crop_info = FamilyCropInfo.objects.create(
            family=family,
            crop_type=crop_type,
            other_crops=self._get_value(response, other_vegetables_xpath),
        )

        for name in self._get_checkbox_values(response, vegetables_xpath):
            instance, _ = Crop.objects.get_or_create(name=name)
            FamilyCrop.objects.create(
                info=family_crop_info,
                crop=instance,
            )

        #==================== START =========================#
        farm_type_xpath = '//*[@name="wpcf[fpis-agri-cvrc-9a-rainfed]"]//option[@selected]/text()'
        irrigated_value_xpath = '//*[@name="wpcf[fpis-agri-cvrc-9b-irrigated]"]/@value'
        marketing_xpath = '//*[contains(@name, "wpcf[fpis-agri-cvrc-10-marketing]")][@checked="checked"]/@value'
        market_outlets_xpath = '//*[contains(@name, "wpcf[fpis-agri-cvrc-11a-marketing-outlet]")][@checked="checked"]/@value'
        other_market_outlets_xpath = '//*[@name="wpcf[fpis-agri-cvrc-11b-other-marketing-outlets]"]/@value'

        crop_type, _ = CropType.objects.get_or_create(name='Commercial and Root Crops')
        family_crop_info = FamilyCropInfo.objects.create(
            family=family,
            crop_type=crop_type,
            farm_type=self._get_choice_value(
                response, farm_type_xpath, FARM_TYPE_CHOICES
            ),
            irrigated_value=self._get_value(response, irrigated_value_xpath),
            other_market_outlets=self._get_value(response, other_market_outlets_xpath),
        )

        for name in self._get_checkbox_values(response, marketing_xpath):
            instance, _ = FarmMarketing.objects.get_or_create(name=name)
            family_crop_info.marketing.add(instance)

        for name in self._get_checkbox_values(response, market_outlets_xpath):
            instance, _ = MarketOutlet.objects.get_or_create(name=name)
            family_crop_info.market_outlets.add(instance)

        crop_names = [
            'Pepper',
            'Cucumber',
            'Cabbage',
            'Carrots',
            'Gabi',
            'Potato',
            'Sweet Potato',
            'Cassava',
        ]

        hills_xpath = '//*[contains(@name, "wpcf[fpis-agri-cvrc-{}c1")]/@value'
        hectares_xpath = '//*[contains(@name, "wpcf[fpis-agri-cvrc-{}c2")]/@value'
        kilos_xpath = '//*[contains(@name, "wpcf[fpis-agri-cvrc-{}c3")]/@value'
        income_xpath = '//*[contains(@name, "wpcf[fpis-agri-cvrc-{}c4")]/@value'
        costs_xpath = '//*[contains(@name, "wpcf[fpis-agri-cvrc-{}c5")]/@value'

        for i in range(1, 9):
            hills = self._get_number_value(response, hills_xpath.format(i))
            hectares = self._get_number_value(response, hectares_xpath.format(i))
            kilos = self._get_number_value(response, kilos_xpath.format(i))
            income = self._get_number_value(response, income_xpath.format(i))
            costs = self._get_number_value(response, costs_xpath.format(i))

            crop_name = crop_names.pop()
            crop, _ = Crop.objects.get_or_create(name=crop_name)

            if any([hectares, hills, income, costs, kilos]):
                FamilyCrop.objects.create(
                    info=family_crop_info,
                    crop=crop,
                    hectares=hectares,
                    hills=hills,
                    kilos_per_cropping=kilos,
                    income_per_cropping=income,
                    production_costs_per_cropping=costs,
                )


        # ==================== START Agroforestry =========================#
        inorganic_bags_xpath = '//*[@name="wpcf[fpis-agri-ac-bags-inorganic-fertilizers]"]/@value'
        organic_bags_xpath = '//*[@name="wpcf[fpis-agri-ac-bags-organic-fertilizers]"]/@value'
        source_bags_xpath = '//*[@name="wpcf[fpis-agri-ac-fertilizers-source]"]/@value'

        marketing_xpath = '//*[contains(@name, "wpcf[fpis-agri-ac-7-marketing]")][@checked="checked"]/@value'
        market_outlets_xpath = '//*[contains(@name, "wpcf[fpis-agri-ac-8a-marketing-outlet]")][@checked="checked"]/@value'
        other_market_outlets_xpath = '//*[@name="wpcf[fpis-agri-ac-8b-other-marketing-outlets]"]/@value'

        crop_type, _ = CropType.objects.get_or_create(name='Agroforestry')
        family_crop_info = FamilyCropInfo.objects.create(
            family=family,
            crop_type=crop_type,
            inorganic_bags=self._get_value(response, inorganic_bags_xpath),
            organic_bags=self._get_value(response, organic_bags_xpath),
            source_bags=self._get_value(response, source_bags_xpath),
            other_market_outlets=self._get_value(response, other_market_outlets_xpath),
        )

        for name in self._get_checkbox_values(response, marketing_xpath):
            instance, _ = FarmMarketing.objects.get_or_create(name=name)
            family_crop_info.marketing.add(instance)

        for name in self._get_checkbox_values(response, market_outlets_xpath):
            instance, _ = MarketOutlet.objects.get_or_create(name=name)
            family_crop_info.market_outlets.add(instance)

        crop_names = [
            'Timber',
            'Rattan',
            'Bamboo',
            'Falcata',
            'Rubber',
        ]

        hills_xpath = '//*[contains(@name, "wpcf[fpis-agri-ac-{}c1")]/@value'
        hectares_xpath = '//*[contains(@name, "wpcf[fpis-agri-ac-{}c2")]/@value'
        income_xpath = '//*[contains(@name, "wpcf[fpis-agri-ac-{}c3")]/@value'
        costs_xpath = '//*[contains(@name, "wpcf[fpis-agri-ac-{}c4")]/@value'

        for i in range(1, len(crop_names)):
            hills = self._get_number_value(response, hills_xpath.format(i))
            hectares = self._get_number_value(response, hectares_xpath.format(i))
            income = self._get_number_value(response, income_xpath.format(i))
            costs = self._get_number_value(response, costs_xpath.format(i))

            crop_name = crop_names.pop()
            crop, _ = Crop.objects.get_or_create(name=crop_name)

            if any([hectares, hills, income, costs]):
                FamilyCrop.objects.create(
                    info=family_crop_info,
                    crop=crop,
                    hectares=hectares,
                    hills=hills,
                    income_per_cropping=income,
                    production_costs_per_cropping=costs,
                )

        # Start Production Economy
        own_consumption_xpath = '//*[contains(@name, "wpcf[fpis-agri-own-consumption]")]/@value'
        keep_records_xpath = '//*[contains(@name, "wpcf[fpis-agri-records-production-cost]")]//option[@selected]/text()'
        highest_cost_xpath = '//*[contains(@name, "wpcf[fpis-agri-highest-cost]")]/@value'
        second_highest_cost_xpath = '//*[contains(@name, "wpcf[fpis-agri-2nd-highest-cost]")]/@value'
        selling_price_xpath = '//*[contains(@name, "wpcf[fpis-agri-selling-price-vs-cost]")]//option[@selected]/text()'
        paid_duration_xpath = '//*[contains(@name, "wpcf[fpis-agri-getting-paid]")]/@value'
        crop_loss_xpath = '//*[contains(@name, "wpcf[fpis-crop-losses]")]//option[@selected]/text()'
        crop_loss_reasons_xpath = '//*[contains(@name, "wpcf[fpis-reasons-crop-losses]")]/@value'

        ProductionEconomy.objects.create(
            family=family,
            own_consumption=self._get_value(response, own_consumption_xpath),
            keep_records=self._get_choice_value(
                response, keep_records_xpath, constants.YES_NO_CHOICES
            ),
            highest_cost=self._get_value(response, highest_cost_xpath),
            second_highest_cost=self._get_value(response, second_highest_cost_xpath),
            selling_price=self._get_foreign_key_value(
                response, selling_price_xpath, SellingPrice
            ),
            paid_duration=self._get_value(response, paid_duration_xpath),
            crop_loss=self._get_choice_value(
                response, crop_loss_xpath, constants.YES_NO_CHOICES
            ),
            crop_loss_reasons=self._get_value(response, crop_loss_reasons_xpath),
        )

        # Start FarmMachinery
        manual_xpath = '//*[contains(@name, "wpcf[fpis-agri-farm-implements-manual]")]/@value'
        mechanical_xpath = '//*[contains(@name, "wpcf[fpis-agri-farm-implements-mechanical]")]/@value'
        owned_drier_xpath = '//*[contains(@name, "wpcf[fpis-agri-owned-drier]")]/@value'
        owned_thresher_xpath = '//*[contains(@name, "wpcf[fpis-agri-owned-thresher]")]/@value'
        others_owned_xpath = '//*[contains(@name, "wpcf[fpis-agri-facilities-owned-others]")]/@value'
        FarmMachinery.objects.create(
            family=family,
            manual=self._get_value(response, manual_xpath),
            mechanical=self._get_value(response, mechanical_xpath),
            owned_drier=self._get_value(response, owned_drier_xpath),
            owned_thresher=self._get_value(response, owned_thresher_xpath),
            others_owned=self._get_value(response, others_owned_xpath),
        )

        # Start MarketInfo
        situation_knowledge_xpath = '//*[contains(@name, "wpcf[fpis-agri-market-price-info]")]/@value'
        is_relevant_xpath = '//*[contains(@name, "wpcf[fpis-agri-market-info-relevant]")]//option[@selected]/text()'
        information_use_xpath = '//*[contains(@name, "wpcf[fpis-agri-market-info-using]")]/@value'
        problems_encountered_xpath = '//*[contains(@name, "wpcf[fpis-agri-farming-problems]")][@checked="checked"]/@value'
        other_problems_xpath = '//*[contains(@name, "wpcf[fpis-agri-farming-problems-others]")]/@value'
        main_problem_xpath = '//*[contains(@name, "wpcf[fpis-agri-main-problem-production]")]/@value'
        trainings_xpath = '//*[contains(@name, "wpcf[fpis-agri-training-seminars]")]/text()'
        waste_practices_xpath = '//*[contains(@name, "wpcf[fpis-agri-23a-farm-waste-recycling-practices]")][@checked="checked"]/@value'
        other_waste_practices_xpath = '//*[contains(@name, "wpcf[fpis-agri-23b-other-farm-waste-recycling-practices]")]/@value'

        market_info = MarketInfo.objects.create(
            family=family,
            situation_knowledge=self._get_value(response, situation_knowledge_xpath),
            is_relevant=self._get_choice_value(
                response, is_relevant_xpath, constants.YES_NO_CHOICES
            ),
            information_use=self._get_value(response, information_use_xpath),
            other_problems=self._get_value(response, other_problems_xpath),
            main_problem=self._get_value(response, main_problem_xpath),
            trainings=self._get_value(response, trainings_xpath),
            other_waste_practices=self._get_value(response, other_waste_practices_xpath),
        )

        for name in self._get_checkbox_values(response, problems_encountered_xpath):
            instance, _ = FarmingProblem.objects.get_or_create(name=name)
            market_info.problems_encountered.add(instance)

        for name in self._get_checkbox_values(response, waste_practices_xpath):
            instance, _ = FarmWastePractice.objects.get_or_create(name=name)
            market_info.waste_practices.add(instance)

        # Start FamilyLiveStock
        sale_income_xpath = '//*[@name="wpcf[fpis-livestock-sale-live-income]"]/@value'
        sale_number_xpath = '//*[@name="wpcf[fpis-livestock-sale-live-number]"]/@value'
        other_income_xpath = '//*[@name="wpcf[fpis-livestock-other-income]"]/@value'

        family_live_stock = FamilyLiveStock.objects.create(
            family=family,
            sale_income=self._get_number_value(response, sale_income_xpath),
            sale_number=self._get_number_value(response, sale_number_xpath),
            other_income=self._get_number_value(response, other_income_xpath),
        )

        animals = [
            ('cattle', 'Cattle'),
            ('carabao', 'Carabao'),
            ('swine-hog', 'Swine / Hog'),
            ('goat', 'Goat'),
            ('sheep', 'Sheep'),
            ('horse', 'Horse'),
            ('rabbit', 'Rabbit'),
        ]
        male_xpath = '//*[@name="wpcf[fpis-livestock-{}-raised-male]"]/@value'
        female_xpath = '//*[@name="wpcf[fpis-livestock-{}-raised-female]"]/@value'
        breed_xpath = '//*[contains(@name, "wpcf[fpis-livestock-{}-breed]")][@checked="checked"]/@value'
        breedable_female_xpath = '//*[@name="wpcf[fpis-livestock-{}-breedable-female]"]/@value'

        for item in animals:
            name = item[1]
            male = self._get_number_value(response, male_xpath.format(item[0]))
            female = self._get_number_value(response, female_xpath.format(item[0]))
            breedable_female = self._get_number_value(response, breedable_female_xpath.format(item[0]))

            if any([male, female, breedable_female]):
                animal, _ = Animal.objects.get_or_create(name=name)
                live_stock = LiveStock.objects.create(
                    info=family_live_stock,
                    animal=animal,
                    male=male,
                    female=female,
                    breedable_female=breedable_female,
                )

                for name in self._get_checkbox_values(response, breed_xpath.format(item[0])):
                    instance, _ = Breed.objects.get_or_create(name=name)
                    live_stock.breeds.add(instance)

        # Start FamilyPetAndPoultry
        pet_animals_xpath = '//*[@name="wpcf[fpis-pet-animals]"]/@value'
        no_of_pet_animals_xpath = '//*[@name="wpcf[fpis-pet-animals-heads-raised]"]/@value'
        is_immunized_xpath = '//*[@name="wpcf[fpis-pet-animals-immunized]"]//option[@selected]/text()'

        family_pet_and_poultry = FamilyPetAndPoultry.objects.create(
            family=family,
            pet_animals=self._get_value(response, pet_animals_xpath),
            no_of_pet_animals=self._get_number_value(response, no_of_pet_animals_xpath),
            is_immunized=self._get_bool_value(
                response, is_immunized_xpath
            ),
        )

        poultries = [
            ('broiler', 'Broiler'),
            ('layer', 'Layer'),
            ('native-chicken', 'Native Chicken'),
        ]

        financing_xpath = '//*[@name="wpcf[fpis-poultry-{}-financing]"]//option[@selected]/text()'
        heads_xpath = '//*[@name="wpcf[fpis-poultry-{}-heads-per-cycle]"]/@value'

        for poultry in poultries:
            Poultry.objects.create(
                info=family_pet_and_poultry,
                name=poultry[1],
                financing=self._get_choice_value(
                    response, financing_xpath.format(poultry[0]), Poultry.FINANCING_CHOICES
                ),
                heads=self._get_number_value(response, heads_xpath.format(poultry[0])),
            )

        poultries = [
            ('ducks', 'Ducks'),
            ('turkey', 'Turkey'),
            ('game-foul', 'Game Foul'),
        ]
        heads_xpath = '//*[@name="wpcf[fpis-{}-heads-raised]"]/@value'
        balut_xpath = '//*[@name="wpcf[fpis-{}-balut-production]"]/@value'

        for poultry in poultries:
            Poultry.objects.create(
                info=family_pet_and_poultry,
                name=poultry[1],
                heads=self._get_number_value(response, heads_xpath.format(poultry[0])),
                balut_production=self._get_number_value(
                    response, balut_xpath.format(poultry[0])
                ),
            )

        # Start Family Pond
        pond_areas_xpath = '//*[contains(@name, "wpcf[fpis-fishery-pond-area]")][@checked="checked"]/@value'
        fish_captures_xpath = '//*[contains(@name, "wpcf[fpis-fishery-fish-capture]")][@checked="checked"]/@value'
        vessel_xpath = '//*[@name="wpcf[fpis-fishery-type-of-vessel]"]//option[@selected]/text()'
        vessel_notes_xpath = '//*[@name="wpcf[fpis-fishery-notes-type-of-vessel]"]/@value'
        is_vessel_registered_xpath = '//*[@name="wpcf[fpis-fishery-vessel-banca-registered]"]//option[@selected]/text()'
        registration_notes_xpath = '//*[@name="wpcf[fpis-fishery-where-registration]"]/@value'
        fishing_gears_xpath = '//*[contains(@name, "wpcf[fpis-fishery-fishing-gear-used]")][@checked="checked"]/@value'
        other_fishing_gears_xpath = '//*[@name="wpcf[fpis-fishery-notes-fishing-gear]"]/@value'
        fish_net_size_xpath = '//*[@name="wpcf[fpis-fishery-size-of-fish-net]"]/@value'
        fad_used_xpath = '//*[@name="wpcf[fpis-fishery-fad-used]"]/@value'

        family_pond = FamilyPond.objects.create(
            family=family,
            vessel=self._get_choice_value(response, vessel_xpath, FamilyPond.VESSEL_CHOICES),
            vessel_notes=self._get_value(response, vessel_notes_xpath),
            is_vessel_registered=self._get_bool_value(response, is_vessel_registered_xpath),
            registration_notes=self._get_value(response, registration_notes_xpath),
            other_fishing_gears=self._get_value(response, other_fishing_gears_xpath),
            fish_net_size=self._get_value(response, fish_net_size_xpath),
            fad_used=self._get_value(response, fad_used_xpath),
        )

        for name in self._get_checkbox_values(response, pond_areas_xpath):
            instance, _ = PondArea.objects.get_or_create(name=name)
            family_pond.pond_areas.add(instance)

        for name in self._get_checkbox_values(response, fish_captures_xpath):
            instance, _ = FishCapture.objects.get_or_create(name=name)
            family_pond.fish_captures.add(instance)

        for name in self._get_checkbox_values(response, fishing_gears_xpath):
            instance, _ = FishingGear.objects.get_or_create(name=name)
            family_pond.fishing_gears.add(instance)

        area_developed_xpath = '//*[@name="wpcf[fpis-fishery-{}c1-pond-area-developed]"]/@value'
        cultured_species_xpath = '//*[@name="wpcf[fpis-fishery-{}c2-cultured-species]"]/@value'
        total_harvest_xpath = '//*[@name="wpcf[fpis-fishery-{}c3-total-harvest]"]/@value'
        volume_sold_xpath = '//*[@name="wpcf[fpis-fishery-{}c4-volume-of-yield-sold]"]/@value'
        total_income_xpath = '//*[@name="wpcf[fpis-fishery-{}c5-total-income]"]/@value'
        total_expenses_xpath = '//*[@name="wpcf[fpis-fishery-{}c6-total-operating-expenses]"]/@value'

        for i in range(2, 6):
            area_developed = self._get_value(response, area_developed_xpath.format(i))
            cultured_species = self._get_value(response, cultured_species_xpath.format(i))
            total_harvest = self._get_number_value(response, total_harvest_xpath.format(i))
            volume_sold = self._get_number_value(response, volume_sold_xpath.format(i))
            total_income = self._get_number_value(response, total_income_xpath.format(i))
            total_expenses = self._get_number_value(response, total_expenses_xpath.format(i))

            if any([area_developed, cultured_species, total_harvest, volume_sold, total_income, total_expenses]):
                Pond.objects.create(
                    info=family_pond,
                    area_developed=area_developed,
                    cultured_species=cultured_species,
                    total_harvest=total_harvest,
                    volume_sold=volume_sold,
                    total_income=total_income,
                    total_expenses=total_expenses,
                )

        # Start FARInventory
        marketing_xpath = '//*[contains(@name, "wpcf[fpis-far-8-marketing-of-farm-produce]")][@checked="checked"]/@value'
        market_outlets_xpath = '//*[contains(@name, "wpcf[fpis-far-9-marketing-outlet]")][@checked="checked"]/@value'
        other_marketing_xpath = '//*[contains(@name, "wpcf[fpis-far-other-marketing-outlet]")]/@value'

        far_inventory = FARInventory.objects.create(
            family=family,
            other_marketing=self._get_value(response, other_marketing_xpath),
        )

        for name in self._get_checkbox_values(response, marketing_xpath):
            instance, _ = FarmMarketing.objects.get_or_create(name=name)
            far_inventory.marketing.add(instance)

        for name in self._get_checkbox_values(response, market_outlets_xpath):
            instance, _ = MarketOutlet.objects.get_or_create(name=name)
            far_inventory.market_outlets.add(instance)

        species_xpath = '//*[@name="wpcf[fpis-far-{}c1-seaweed-farming-species]"]/@value'
        hectares_xpath = '//*[@name="wpcf[fpis-far-{}c2-no-of-hectares-planted]"]/@value'
        average_yield_xpath = '//*[@name="wpcf[fpis-far-{}c3-average-yield]"]/@value'
        fresh_sold_xpath = '//*[@name="wpcf[fpis-far-{}c4-sold-fresh-in-tons]"]/@value'
        dried_sold_xpath = '//*[@name="wpcf[fpis-far-{}c5-sold-dried-in-tons]"]/@value'
        fresh_income_xpath = '//*[@name="wpcf[fpis-far-{}c6-income-fresh]"]/@value'
        dried_income_xpath = '//*[@name="wpcf[fpis-far-{}c7-income-dried]"]/@value'

        for i in range(1, 5):
            species = self._get_value(response, species_xpath.format(i))
            hectares = self._get_number_value(response, hectares_xpath.format(i))
            average_yield = self._get_number_value(response, average_yield_xpath.format(i))
            fresh_sold = self._get_number_value(response, fresh_sold_xpath.format(i))
            dried_sold = self._get_number_value(response, dried_sold_xpath.format(i))
            fresh_income = self._get_number_value(response, fresh_income_xpath.format(i))
            dried_income = self._get_number_value(response, dried_income_xpath.format(i))

            if any([species, hectares, average_yield, fresh_sold, dried_sold, fresh_income, dried_income]):
                FishAndAquatic.objects.create(
                    type=FishAndAquatic.TYPE_SEAWEED,
                    info=far_inventory,
                    species=species,
                    hectares=hectares,
                    average_yield=average_yield,
                    fresh_sold=fresh_sold,
                    dried_sold=dried_sold,
                    fresh_income=fresh_income,
                    dried_income=dried_income,
                )

        # TYPE_OYSTER
        species_xpath = '//*[@name="wpcf[fpis-far-{}c1-oyster-clam-culture-species]"]/@value'
        hectares_xpath = '//*[@name="wpcf[fpsi-far-{}c2-no-of-hectares-planted]"]/@value'
        average_yield_xpath = '//*[@name="wpcf[fpis-far-{}c3-average-yield]"]/@value'
        fresh_sold_xpath = '//*[@name="wpcf[fpis-far-{}c4-sold-fresh-in-tons]"]/@value'
        dried_sold_xpath = '//*[@name="wpcf[fpis-far-{}c5-sold-dried-in-tons]"]/@value'
        fresh_income_xpath = '//*[@name="wpcf[fpis-far-{}c6-income-fresh]"]/@value'
        dried_income_xpath = '//*[@name="wpcf[fpis-far-{}c7-income-dried]"]/@value'

        for i in range(5, 8):
            species = self._get_value(response, species_xpath.format(i))
            hectares = self._get_number_value(response, hectares_xpath.format(i))
            average_yield = self._get_number_value(response, average_yield_xpath.format(i))
            fresh_sold = self._get_number_value(response, fresh_sold_xpath.format(i))
            dried_sold = self._get_number_value(response, dried_sold_xpath.format(i))
            fresh_income = self._get_number_value(response, fresh_income_xpath.format(i))
            dried_income = self._get_number_value(response, dried_income_xpath.format(i))

            if any([species, hectares, average_yield, fresh_sold, dried_sold, fresh_income, dried_income]):
                FishAndAquatic.objects.create(
                    type=FishAndAquatic.TYPE_OYSTER_CLAM,
                    info=far_inventory,
                    species=species,
                    hectares=hectares,
                    average_yield=average_yield,
                    fresh_sold=fresh_sold,
                    dried_sold=dried_sold,
                    fresh_income=fresh_income,
                    dried_income=dried_income,
                )

        # Start FarmerOrganization
        has_membership_xpath = '//*[@name="wpcf[fpsi-fo-1a-membership-organization]"]//option[@selected]/text()'
        name_xpath = '//*[@name="wpcf[fpsi-fo-membership-organization-name]"]/@value'
        has_position_xpath = '//*[@name="wpcf[fpsi-fo-2a-position-organization]"]//option[@selected]/text()'
        position_reason_xpath = '//*[@name="wpcf[fpsi-fo-2b-if-no-why]"]/@value'
        is_active_xpath = '//*[@name="wpcf[fpsi-fo-3a-status-organization]"]//option[@selected]/text()'
        years_of_existence_xpath = '//*[@name="wpcf[fpsi-fo-3b-years-in-existince]"]/@value'
        inactive_duration_xpath = '//*[@name="wpcf[fpsi-fo-inactive-how-long]"]/@value'
        num_of_members_xpath = '//*[@name="wpcf[fpsi-fo-3d-number-members]"]/@value'
        services_provided_xpath = '//*[@name="wpcf[fpsi-fo-4a-services-provided]"]/@value'
        services_availed_xpath = '//*[@name="wpcf[fpsi-fo-4b-services-availed]"]/@value'
        times_used_xpath = '//*[@name="wpcf[fpsi-fo-5a-use-their-services]"]/@value'
        participates_in_activities_xpath = '//*[@name="wpcf[fpsi-fo-activities-meeting]"]//option[@selected]/text()'
        knows_management_team_xpath = '//*[@name="wpcf[fpsi-fo-6a-know-management-team]"]//option[@selected]/text()'
        trusts_management_xpath = '//*[@name="wpcf[fpsi-fo-6b-trust-them]"]//option[@selected]/text()'
        thoughts_xpath = '//*[contains(@name, "wpcf[fpsi-fo-7-what-you-think]")][@checked="checked"]/@value'
        meeting_xpath = '//*[@name="wpcf[fpsi-fo-meet-technician]"]//option[@selected]/text()'
        meeting_frequency_xpath = '//*[@name="wpcf[fpsi-fo-9-frequency]"]//option[@selected]/text()'
        usefullness_xpath = '//*[@name="wpcf[fpsi-fo-10-visit-useful]"]//option[@selected]/text()'
        agricultural_services_xpath = '//*[@name="wpcf[fpsi-fo-agricultural-services]"]/@value'

        farmer_org = FarmerOrganization.objects.create(
            family=family,
            has_membership=self._get_bool_value(response, has_membership_xpath),
            name=self._get_value(response, name_xpath),
            has_position=self._get_bool_value(response, has_position_xpath),
            position_reason=self._get_value(response, position_reason_xpath),
            is_active=self._get_bool_value(response, is_active_xpath),
            years_of_existence=self._get_value(response, years_of_existence_xpath),
            inactive_duration=self._get_value(response, inactive_duration_xpath),
            num_of_members=self._get_number_value(response, num_of_members_xpath),
            services_provided=self._get_value(response, services_provided_xpath),
            services_availed=self._get_value(response, services_availed_xpath),
            times_used=self._get_value(response, times_used_xpath),
            participates_in_activities=self._get_bool_value(
                response, participates_in_activities_xpath
            ),
            knows_management_team=self._get_bool_value(response, knows_management_team_xpath),
            trusts_management=self._get_bool_value(response, trusts_management_xpath),
            meeting=self._get_choice_value(
                response, meeting_xpath, FarmerOrganization.MEETING_CHOICES
            ),
            meeting_frequency=self._get_choice_value(
                response, meeting_frequency_xpath, FarmerOrganization.FREQ_CHOICES
            ),
            usefullness=self._get_choice_value(
                response, usefullness_xpath, FarmerOrganization.USEFUL_CHOICES
            ),
            agricultural_services=self._get_value(response, agricultural_services_xpath),
        )

        for name in self._get_checkbox_values(response, thoughts_xpath):
            instance, _ = OrgThought.objects.get_or_create(name=name)
            farmer_org.thoughts.add(instance)

        # Start Psychosocial
        violence_against_women_xpath = '//*[@name="wpcf[fpis-fpsc-1a-voilence-against-women]"]//option[@selected]/text()'
        num_of_violence_against_women_xpath = '//*[@name="wpcf[fpis-fpsc-1a-number-of-episodes]"]/@value'
        violence_against_children_xpath = '//*[@name="wpcf[fpis-fpsc-2a-voilence-against-children]"]//option[@selected]/text()'
        num_of_violence_against_children_xpath = '//*[@name="wpcf[fpis-fpsc-2b-number-of-episodes]"]/@value'
        conflict_with_law_xpath = '//*[@name="wpcf[fpis-fpsc-3-conflict-with-law]"]//option[@selected]/text()'
        victims_of_armed_conflict_xpath = '//*[@name="wpcf[fpis-fpsc-4-armed-conflict]"]//option[@selected]/text()'
        practice_family_planning_xpath = '//*[@name="wpcf[fpis-fpsc-5a-family-planning]"]//option[@selected]/text()'
        family_planning_method_xpath = '//*[@name="wpcf[fpis-fpsc-5b-method-used]"]/@value'
        availed_services_xpath = '//*[contains(@name, "wpcf[fpis-fpsc-6-availed-services-grants]")][@checked="checked"]/@value'
        is_national_gov_helping_xpath = '//*[@name="wpcf[fpis-fpsc-8-national-government]"]//option[@selected]/text()'
        is_provincial_gov_helping_xpath = '//*[@name="wpcf[fpis-fpsc-9-provincial-government]"]//option[@selected]/text()'
        is_municipal_gov_helping_xpath = '//*[@name="wpcf[fpis-fpsc-10-municipal-government]"]//option[@selected]/text()'
        is_barangay_gov_helping_xpath = '//*[@name="wpcf[fpis-fpsc-8-barangay-government]"]//option[@selected]/text()'
        name_of_respondent_xpath = '//*[@name="wpcf[fpis-fpsc-12-respondent]"]/@value'
        name_of_enumerator_xpath = '//*[@name="wpcf[fpis-fpsc-13-enumerator]"]/@value'

        psycho_social = PsychoSocial.objects.create(
            family=family,
            violence_against_women=self._get_bool_value(response, violence_against_women_xpath),
            num_of_violence_against_women=self._get_value(response, num_of_violence_against_women_xpath),
            violence_against_children=self._get_bool_value(response, violence_against_children_xpath),
            num_of_violence_against_children=self._get_value(response, num_of_violence_against_children_xpath),
            conflict_with_law=self._get_bool_value(response, conflict_with_law_xpath),
            victims_of_armed_conflict=self._get_bool_value(response, victims_of_armed_conflict_xpath),
            practice_family_planning=self._get_bool_value(response, practice_family_planning_xpath),
            family_planning_method=self._get_value(response, family_planning_method_xpath),
            is_national_gov_helping=self._get_choice_value(
                response, is_national_gov_helping_xpath, PsychoSocial.HELP_CHOICES
            ),
            is_provincial_gov_helping=self._get_choice_value(
                response, is_provincial_gov_helping_xpath, PsychoSocial.HELP_CHOICES
            ),
            is_municipal_gov_helping=self._get_choice_value(
                response, is_municipal_gov_helping_xpath, PsychoSocial.HELP_CHOICES
            ),
            is_barangay_gov_helping=self._get_choice_value(
                response, is_barangay_gov_helping_xpath, PsychoSocial.HELP_CHOICES
            ),
            name_of_respondent=self._get_value(response, name_of_respondent_xpath),
            name_of_enumerator=self._get_value(response, name_of_enumerator_xpath),
        )

        for name in self._get_checkbox_values(response, availed_services_xpath):
            instance, _ = ServiceGrant.objects.get_or_create(name=name)
            psycho_social.availed_services.add(instance)

        codes = [
            ('wpcf[fpis-fpsc-7a-hearing]', 'Hearing'),
            ('wpcf[fpis-fpsc-7b-visual]', 'Visual'),
            ('wpcf[fpis-fpsc-7c-speech]', 'Speech'),
            ('wpcf[fpis-fpsc-7d-physical]', 'Pysical'),
            ('wpcf[fpis-fpsc-7e-harelip]', 'Harelip'),
            ('wpcf[fpis-fpsc-7f-mental-disability]', 'Mental'),
            ('wpcf[fpis-fpsc-7b-multiple-disabilities]', 'Multiple Disabilities'),
        ]

        for code in codes:
            value = self._get_number_value(response, '//*[@name="{}"]/@value'.format(code[0]))

            if value:
                Disability.objects.create(
                    family=family,
                    name=code[1],
                    number=value,
                )

        print('Wordpress ID: {} - Django ID: {} - Name: {}'.format(wordpress_id, family.id, family.head.name))
