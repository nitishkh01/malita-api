FROM python:3.6
ENV PYTHONUNBUFFERED 1

MAINTAINER Riel

RUN apt-get update && \
    apt-get install -y && \
    mkdir /code && \
    mkdir /srv/logs && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY ./requirements/ /code/requirements/

WORKDIR /code

RUN pip install -r requirements/base.txt && \
  rm -rf ~/.cache/pip

RUN pip install -r requirements/base.txt

COPY . /code/
