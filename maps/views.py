from django.views.generic.base import TemplateView

from families.models import Family


class MapView(TemplateView):

    template_name = 'maps/index.html'

    def get_context_data(self, **kwargs):
        context = super(MapView, self).get_context_data(**kwargs)
        families = Family.objects.search(self.request.GET.get('search', ''))
        families = families.exclude(map_points='').values(
            'map_points',
            'head__name',
        )
        context.update({
            'families': families,
        })
        return context
