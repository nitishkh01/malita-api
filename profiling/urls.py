"""profiling URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.decorators.clickjacking import xframe_options_exempt

from rest_framework.routers import DefaultRouter

from education.views import (
    EducationalProfileViewSet,
)
from housing.views import FamilyHousingViewSet
from income.views import (
    FamilyAccessViewSet,
    FamilyAssetsViewSet,
    FamilyExpendituresViewSet,
    FamilyIncomeViewSet,
)
from maps.views import MapView
from nutrition.views import NutritionProfileViewSet

router = DefaultRouter()
router.register(r'education-profiles', EducationalProfileViewSet)
router.register(r'housing', FamilyHousingViewSet)
router.register(r'income', FamilyIncomeViewSet)
router.register(r'expenditures', FamilyExpendituresViewSet)
router.register(r'assets', FamilyAssetsViewSet)
router.register(r'access', FamilyAccessViewSet)
router.register(r'nutrition-profiles', NutritionProfileViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^admin/', admin.site.urls),
    url(r'^agriculture/', include('agriculture.urls')),
    url(r'^maps/$', xframe_options_exempt(MapView.as_view())),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^families/', include('families.urls')),
    url(r'^fisheries/', include('fisheries.urls')),
    url(r'^graphs/', include('graphs.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
