from django.contrib import admin

from .models import (
    FamilyCrop,
    FamilyCropInfo,
    FamilyFarm,
    FamilyLiveStock,
    FamilyPetAndPoultry,
    FarmLabor,
    FarmMachinery,
    FarmerOrganization,
    LiveStock,
    MarketInfo,
    Poultry,
    ProductionEconomy,
)


class FarmLaborInlineAdmin(admin.TabularInline):
    model = FarmLabor
    extra = 1


@admin.register(FamilyFarm)
class FamilyFarmAdmin(admin.ModelAdmin):
    inlines = [
        FarmLaborInlineAdmin,
    ]
    filter_horizontal = [
        'workers',
        'waste_practices',
    ]


class FamilyCropInlineAdmin(admin.TabularInline):
    model = FamilyCrop
    extra = 1


@admin.register(FamilyCropInfo)
class FamilyCropInfoAdmin(admin.ModelAdmin):
    inlines = [
        FamilyCropInlineAdmin,
    ]


class LiveStockInlineAdmin(admin.TabularInline):
    model = LiveStock
    extra = 1


@admin.register(FamilyLiveStock)
class FamilyLiveStock(admin.ModelAdmin):
    inlines = [
        LiveStockInlineAdmin,
    ]


class PoultryInlineAdmin(admin.TabularInline):
    model = Poultry
    extra = 1


@admin.register(FamilyPetAndPoultry)
class FamilyPetAndPoultryAdmin(admin.ModelAdmin):
    inlines = [
        PoultryInlineAdmin,
    ]


admin.site.register(FarmMachinery)
admin.site.register(ProductionEconomy)
admin.site.register(MarketInfo)
admin.site.register(FarmerOrganization)
