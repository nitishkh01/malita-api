from rest_framework import (
    viewsets,
)
from .models import (
    FamilyCropInfo,
    FamilyFarm,
    FamilyLiveStock,
    FamilyPetAndPoultry,
    FarmerOrganization,
)
from .serializers import (
    FamilyCropInfoSerializer,
    FamilyFarmSerializer,
    FamilyLiveStockSerializer,
    FamilyPetAndPoultrySerializer,
    FarmerOrganizationSerializer,
)


class FamilyFarmViewSet(viewsets.ModelViewSet):
    queryset = FamilyFarm.objects.select_related(
        'tenurial_instrument',
        'farming_practice',
    )
    serializer_class = FamilyFarmSerializer


class FamilyCropInfoViewSet(viewsets.ModelViewSet):
    queryset = FamilyCropInfo.objects.prefetch_related(
        'marketing',
        'market_outlets',
        'financing_practices',
        'familycrop_set',
    )
    serializer_class = FamilyCropInfoSerializer


class FamilyLiveStockViewSet(viewsets.ModelViewSet):
    queryset = FamilyLiveStock.objects.prefetch_related(
        'livestock_set',
    )
    serializer_class = FamilyLiveStockSerializer


class FamilyPetAndPoultryViewSet(viewsets.ModelViewSet):
    queryset = FamilyPetAndPoultry.objects.prefetch_related(
        'poultry_set',
    )
    serializer_class = FamilyPetAndPoultrySerializer


class FarmerOrganizationViewSet(viewsets.ModelViewSet):
    queryset = FarmerOrganization.objects.prefetch_related(
        'thoughts',
    )
    serializer_class = FarmerOrganizationSerializer
