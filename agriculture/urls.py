from django.conf.urls import include, url

from rest_framework.routers import DefaultRouter

from .views import (
    FamilyCropInfoViewSet,
    FamilyFarmViewSet,
    FamilyLiveStockViewSet,
    FamilyPetAndPoultryViewSet,
    FarmerOrganizationViewSet,
)

router = DefaultRouter()
router.register(r'family-farms', FamilyFarmViewSet)
router.register(r'family-livestocks', FamilyLiveStockViewSet)
router.register(r'family-crops', FamilyCropInfoViewSet)
router.register(r'family-pet-and-poultries', FamilyPetAndPoultryViewSet)
router.register(r'farmer-organizations', FarmerOrganizationViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
]
