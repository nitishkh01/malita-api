from django.db import models

from families.constants import (
    YES_NO_CHOICES,
    FARM_TYPE_CHOICES,
    MEETING_CHOICES,
    FREQ_CHOICES,
    USEFUL_CHOICES
)


class TenurialInstrument(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'tenurial instrument'
        verbose_name = 'tenurial instruments'

    def __str__(self):
        return self.name


class FarmingPractice(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'farming practice'
        verbose_name = 'farming practices'

    def __str__(self):
        return self.name


class FarmWorker(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'farm worker'
        verbose_name = 'farm workers'

    def __str__(self):
        return self.name


class FarmWastePractice(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'farm waste recycling practice'
        verbose_name = 'farm waste recycling practices'

    def __str__(self):
        return self.name


class FamilyFarm(models.Model):
    family = models.OneToOneField('families.Family', related_name='farm')
    total_area = models.CharField(max_length=100, blank=True)
    other_areas = models.CharField(max_length=100, blank=True)
    owned = models.CharField(max_length=100, blank=True)
    rented = models.CharField(max_length=100, blank=True)
    tenant = models.CharField(max_length=100, blank=True)
    mortgaged = models.CharField(max_length=100, blank=True)
    martgaged_acquired = models.CharField(max_length=100, blank=True)
    tenurial_instrument = models.ForeignKey('TenurialInstrument', on_delete=models.CASCADE, blank=True, null=True)
    total_land_area = models.CharField(max_length=100, blank=True)
    total_land_area_others = models.CharField(max_length=100, blank=True)
    farming_practice = models.ForeignKey('FarmingPractice', on_delete=models.CASCADE, blank=True, null=True)
    farming_practice_others = models.CharField(max_length=200, blank=True)
    workers = models.ManyToManyField('FarmWorker')
    other_workers = models.CharField(max_length=100, blank=True)
    waste_practices = models.ManyToManyField('FarmWastePractice')
    other_waste_practices = models.CharField(max_length=100, blank=True)
    pakyaw_or_labor = models.CharField(max_length=100, blank=True, default='', choices=YES_NO_CHOICES)
    pakyaw_value = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = 'family farm'
        verbose_name_plural = 'family farms'

    def __str__(self):
        return str(self.family)


class FarmLabor(models.Model):
    family_farm = models.ForeignKey('FamilyFarm', on_delete=models.CASCADE)
    type = models.CharField(max_length=100, blank=True)
    rate_per_hour = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = 'family labor'
        verbose_name_plural = 'family labors'

    def __str__(self):
        return str(self.family_farm)


class FarmMarketing(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'marketing of farm produce'
        verbose_name = 'marketing of farm produces'

    def __str__(self):
        return self.name


class MarketOutlet(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'market outlet'
        verbose_name = 'market outlets'

    def __str__(self):
        return self.name


class FinancingPractice(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'financing practice'
        verbose_name = 'financing practices'

    def __str__(self):
        return self.name


class CropType(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'crop type'
        verbose_name_plural = 'crop types'

    def __str__(self):
        return self.name


class Crop(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'crop'
        verbose_name_plural = 'crops'

    def __str__(self):
        return self.name


class FamilyCrop(models.Model):
    info = models.ForeignKey('FamilyCropInfo', on_delete=models.CASCADE)
    crop = models.ForeignKey('Crop', on_delete=models.CASCADE)
    hills = models.PositiveIntegerField(blank=True, null=True)
    hectares = models.FloatField(blank=True, null=True, help_text='No. of hectares')
    kilos_per_cropping = models.FloatField(blank=True, null=True, help_text='Kilos/Tons per Cropping')
    income_per_cropping = models.FloatField(blank=True, null=True)
    production_costs_per_cropping = models.FloatField(blank=True, null=True)

    class Meta:
        verbose_name = 'family crop'
        verbose_name_plural = 'family crops'

    def __str__(self):
        return str(self.crop)


class FamilyCropInfo(models.Model):
    family = models.ForeignKey('families.Family', related_name='crops')
    crop_type = models.ForeignKey('CropType')
    farm_type = models.CharField(max_length=100, blank=True, default='', choices=FARM_TYPE_CHOICES)
    irrigated_value = models.CharField(max_length=100, blank=True)
    inorganic_bags = models.CharField(max_length=100, blank=True)
    organic_bags = models.CharField(max_length=100, blank=True)
    source_bags = models.CharField(max_length=100, blank=True)
    marketing = models.ManyToManyField('FarmMarketing')
    market_outlets = models.ManyToManyField('MarketOutlet')
    other_market_outlets = models.CharField(max_length=100, blank=True)
    applies_microbial = models.CharField(max_length=100, blank=True, default='', choices=YES_NO_CHOICES)
    microbial_reason = models.CharField(max_length=100, blank=True)
    financing_practices = models.ManyToManyField('FinancingPractice')
    seed_types = models.ManyToManyField('SeedType')
    other_seed_types = models.CharField(max_length=100, blank=True)
    other_crops = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return '{} - {}'.format(self.family, self.crop_type)


class FamilyRiceCrop(models.Model):
    family = models.ForeignKey('families.Family', on_delete=models.CASCADE)
    hybrid_hectares = models.FloatField(blank=True, null=True)
    hybrid_kilos = models.FloatField(blank=True, null=True)
    hybrid_income = models.FloatField(blank=True, null=True)
    hybrid_cost = models.FloatField(blank=True, null=True)
    upload_organic_hectares = models.FloatField(blank=True, null=True)
    upload_organic_kilos = models.FloatField(blank=True, null=True)
    upload_organic_income = models.FloatField(blank=True, null=True)
    upload_organic_cost = models.FloatField(blank=True, null=True)
    lowland_inbred_hectares = models.FloatField(blank=True, null=True)
    lowland_inbred_kilos = models.FloatField(blank=True, null=True)
    lowland_inbred_income = models.FloatField(blank=True, null=True)
    lowland_inbred_cost = models.FloatField(blank=True, null=True)
    lowland_organic_hectares = models.FloatField(blank=True, null=True)
    lowland_organic_kilos = models.FloatField(blank=True, null=True)
    lowland_organic_income = models.FloatField(blank=True, null=True)
    lowland_organic_cost = models.FloatField(blank=True, null=True)
    farm_type = models.CharField(max_length=100, blank=True, default='', choices=FARM_TYPE_CHOICES)
    irrigated_value = models.CharField(max_length=100, blank=True)
    inorganic_bags = models.CharField(max_length=100, blank=True)
    organic_bags = models.CharField(max_length=100, blank=True)
    source_bags = models.CharField(max_length=100, blank=True)
    marketing = models.ManyToManyField('FarmMarketing')
    market_outlets = models.ManyToManyField('MarketOutlet')
    applies_microbial = models.CharField(max_length=100, blank=True, default='', choices=YES_NO_CHOICES)
    microbial_reason = models.CharField(max_length=100, blank=True)
    financing_practices = models.ManyToManyField('FinancingPractice')

    class Meta:
        verbose_name = 'staple crop (corn)'
        verbose_name = 'staple crop (corn)'


class SeedType(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'seed type'
        verbose_name = 'seed types'

    def __str__(self):
        return self.name


class FamilyCornCrop(models.Model):
    TYPE_RAINFED = 'rainfed'
    TYPE_IRRIGATED = 'irrigated'
    TYPE_CHOICES = (
        (TYPE_RAINFED, 'Rainfed'),
        (TYPE_IRRIGATED, 'Irrigated'),
    )

    family = models.ForeignKey('families.Family', on_delete=models.CASCADE)
    yellow_corn_hectares = models.FloatField(blank=True, null=True)
    yellow_corn_kilos = models.FloatField(blank=True, null=True)
    yellow_corn_income = models.FloatField(blank=True, null=True)
    yellow_corn_cost = models.FloatField(blank=True, null=True)
    white_corn_hectares = models.FloatField(blank=True, null=True)
    white_corn_kilos = models.FloatField(blank=True, null=True)
    white_corn_income = models.FloatField(blank=True, null=True)
    white_corn_cost = models.FloatField(blank=True, null=True)
    others_adlay_hectares = models.FloatField(blank=True, null=True)
    others_adlay_kilos = models.FloatField(blank=True, null=True)
    others_adlay_income = models.FloatField(blank=True, null=True)
    others_adlay_cost = models.FloatField(blank=True, null=True)
    inorganic_bags = models.CharField(max_length=100, blank=True)
    organic_bags = models.CharField(max_length=100, blank=True)
    source_bags = models.CharField(max_length=100, blank=True)
    seed_types = models.ManyToManyField('SeedType')
    other_types = models.CharField(max_length=100, blank=True)
    marketing = models.ManyToManyField('FarmMarketing')
    market_outlets = models.ManyToManyField('MarketOutlet')
    financing_practices = models.ManyToManyField('FinancingPractice')

    class Meta:
        verbose_name = 'staple crop (corn)'
        verbose_name = 'staple crop (corn)'


class HighValueCrop(models.Model):
    family = models.ForeignKey('families.Family', on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    hills = models.PositiveIntegerField(blank=True, null=True)
    hectares = models.FloatField(blank=True, null=True)
    income = models.FloatField(blank=True, null=True)
    costs = models.FloatField(blank=True, null=True)

    class Meta:
        verbose_name = 'high value crop'
        verbose_name = 'high value crops'

    def __str__(self):
        return self.name


class Vegetable(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'vegetable and root crop'
        verbose_name = 'vegetables and root crops'

    def __str__(self):
        return self.name


class FamilyHighValueCrop(models.Model):
    family = models.ForeignKey('families.Family', on_delete=models.CASCADE)
    marketing = models.ManyToManyField('FarmMarketing')
    market_outlets = models.ManyToManyField('MarketOutlet')
    other_marketing = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = "family's high value crop"
        verbose_name = "family's high value crops"

    def __str__(self):
        return str(self.family)


class FamilyVegetable(models.Model):
    family = models.OneToOneField('families.Family', on_delete=models.CASCADE)
    vegetables = models.ManyToManyField('Vegetable')
    others = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = "family's vegetable and root crop"
        verbose_name = "family's vegetables and root crops"

    def __str__(self):
        return str(self.family)


class CommercialCrop(models.Model):
    family = models.ForeignKey('families.Family', on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    hills = models.PositiveIntegerField(blank=True, null=True)
    kilos = models.FloatField(blank=True, null=True)
    hectares = models.FloatField(blank=True, null=True)
    income = models.FloatField(blank=True, null=True)
    costs = models.FloatField(blank=True, null=True)

    class Meta:
        verbose_name = 'commercial crop'
        verbose_name = 'commercial crops'

    def __str__(self):
        return self.name


class FamilyCommercialCrop(models.Model):
    family = models.OneToOneField('families.Family', on_delete=models.CASCADE)
    farm_type = models.CharField(max_length=100, blank=True, default='', choices=FARM_TYPE_CHOICES)
    irrigated_value = models.CharField(max_length=100, blank=True)
    marketing = models.ManyToManyField('FarmMarketing')
    market_outlets = models.ManyToManyField('MarketOutlet')
    other_marketing = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = "family's commercial crop"
        verbose_name = "family's commercial crops"

    def __str__(self):
        return str(self.family)


class Agroforestry(models.Model):
    family = models.ForeignKey('families.Family', on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    hills = models.PositiveIntegerField(blank=True, null=True)
    hectares = models.FloatField(blank=True, null=True)
    income = models.FloatField(blank=True, null=True)
    costs = models.FloatField(blank=True, null=True)

    class Meta:
        verbose_name = 'agroforestry'
        verbose_name = 'agroforestries'

    def __str__(self):
        return self.name


class FamilyAgroforestry(models.Model):
    family = models.OneToOneField('families.Family', on_delete=models.CASCADE)
    inorganic_bags = models.CharField(max_length=100, blank=True)
    organic_bags = models.CharField(max_length=100, blank=True)
    source_bags = models.CharField(max_length=100, blank=True)
    marketing = models.ManyToManyField('FarmMarketing')
    market_outlets = models.ManyToManyField('MarketOutlet')
    other_marketing = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = "family's commercial crop"
        verbose_name = "family's commercial crops"

    def __str__(self):
        return str(self.family)


class SellingPrice(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'selling price'
        verbose_name = 'selling prices'

    def __str__(self):
        return self.name


class ProductionEconomy(models.Model):
    family = models.OneToOneField('families.Family', on_delete=models.CASCADE)
    own_consumption = models.CharField(max_length=100, blank=True)
    keep_records = models.CharField(max_length=100, blank=True, default='', choices=YES_NO_CHOICES)
    highest_cost = models.CharField(max_length=100, blank=True)
    second_highest_cost = models.CharField(max_length=100, blank=True)
    selling_price = models.ForeignKey('SellingPrice', on_delete=models.CASCADE, blank=True, null=True)
    paid_duration = models.CharField(max_length=100, blank=True)
    crop_loss = models.CharField(max_length=100, blank=True, default='', choices=YES_NO_CHOICES)
    crop_loss_reasons = models.CharField(max_length=200, blank=True)

    class Meta:
        verbose_name = "family's economy of production"
        verbose_name = "family's economy of productions"

    def __str__(self):
        return str(self.family)


class FarmMachinery(models.Model):
    family = models.OneToOneField('families.Family', on_delete=models.CASCADE)
    manual = models.CharField(max_length=200, blank=True)
    mechanical = models.CharField(max_length=200, blank=True)
    owned_drier = models.CharField(max_length=100, blank=True)
    owned_thresher = models.CharField(max_length=100, blank=True)
    others_owned = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = "family's farm implements and machinery"
        verbose_name = "family's farm implements and machineries"

    def __str__(self):
        return str(self.family)


class FarmingProblem(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'farming problem'
        verbose_name = 'farming problems'

    def __str__(self):
        return self.name


class MarketInfo(models.Model):
    family = models.OneToOneField('families.Family', on_delete=models.CASCADE)
    situation_knowledge = models.CharField(max_length=200, blank=True)
    is_relevant = models.CharField(max_length=100, blank=True, default='', choices=YES_NO_CHOICES)
    information_use = models.CharField(max_length=200, blank=True)
    problems_encountered = models.ManyToManyField('FarmingProblem')
    other_problems = models.CharField(max_length=200, blank=True)
    main_problem = models.CharField(max_length=200, blank=True)
    trainings = models.TextField(blank=True)
    waste_practices = models.ManyToManyField('FarmWastePractice')
    other_waste_practices = models.CharField(max_length=100, blank=True)


class Breed(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'breed'
        verbose_name = 'breeds'

    def __str__(self):
        return self.name


class Animal(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'animal'
        verbose_name = 'animals'

    def __str__(self):
        return self.name


class LiveStock(models.Model):
    info = models.ForeignKey('FamilyLiveStock', on_delete=models.CASCADE)
    animal = models.ForeignKey('Animal', on_delete=models.CASCADE)
    male = models.PositiveIntegerField(blank=True, null=True)
    female = models.PositiveIntegerField(blank=True, null=True)
    breeds = models.ManyToManyField('Breed')
    breedable_female = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        verbose_name = 'livestock'
        verbose_name = 'livestocks'

    def __str__(self):
        return str(self.animal)


class FamilyLiveStock(models.Model):
    family = models.OneToOneField('families.Family', on_delete=models.CASCADE)
    sale_income = models.FloatField(blank=True, null=True)
    sale_number = models.FloatField(blank=True, null=True)
    other_income = models.FloatField(blank=True, null=True)

    class Meta:
        verbose_name = "family's livestock"
        verbose_name = "family's livestocks"

    def __str__(self):
        return str(self.family)


class Poultry(models.Model):
    FINANCING_CONTRACT = 'contract'
    FINANCING_SELF = 'self-financed'
    FINANCING_CHOICES = (
        (FINANCING_CONTRACT, 'Contract'),
        (FINANCING_SELF, 'Self Financed'),
    )
    info = models.ForeignKey('FamilyPetAndPoultry', on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    financing = models.CharField(max_length=100, blank=True, default='', choices=FINANCING_CHOICES)
    heads = models.PositiveIntegerField(blank=True, null=True)
    balut_production = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        verbose_name = 'poultry'
        verbose_name = 'poultries'

    def __str__(self):
        return self.name


class FamilyPetAndPoultry(models.Model):
    family = models.OneToOneField('families.Family', on_delete=models.CASCADE)
    pet_animals = models.CharField(max_length=100)
    no_of_pet_animals = models.PositiveIntegerField(blank=True, null=True)
    is_immunized = models.NullBooleanField(blank=True, null=True)

    class Meta:
        verbose_name = "family's pet and poultry"
        verbose_name = "family's pets and poultries"

    def __str__(self):
        return str(self.family)


class OrgThought(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'organization thought'
        verbose_name = 'organization thoughts'

    def __str__(self):
        return self.name


class FarmerOrganization(models.Model):
    family = models.OneToOneField('families.Family', related_name='farmer_org', on_delete=models.CASCADE)
    has_membership = models.NullBooleanField(blank=True, null=True)
    name = models.CharField(max_length=100, blank=True)
    has_position = models.NullBooleanField(blank=True, null=True)
    position_reason = models.CharField(max_length=100, blank=True)
    is_active = models.NullBooleanField(blank=True, null=True)
    years_of_existence = models.CharField(max_length=100, blank=True, null=True)
    inactive_duration = models.CharField(max_length=100, blank=True, null=True)
    num_of_members = models.PositiveIntegerField(blank=True, null=True)
    services_provided = models.CharField(max_length=200, blank=True)
    services_availed = models.CharField(max_length=200, blank=True)
    times_used = models.CharField(max_length=100, blank=True, null=True)
    participates_in_activities = models.NullBooleanField(blank=True, null=True)
    knows_management_team = models.NullBooleanField(blank=True, null=True)
    trusts_management = models.NullBooleanField(blank=True, null=True)
    thoughts = models.ManyToManyField('OrgThought')
    meeting = models.CharField(max_length=100, blank=True, default='', choices=MEETING_CHOICES)
    meeting_frequency = models.CharField(max_length=100, blank=True, default='', choices=FREQ_CHOICES)
    usefullness = models.CharField(max_length=100, blank=True, default='', choices=USEFUL_CHOICES)
    agricultural_services = models.CharField(max_length=200, blank=True)

    class Meta:
        verbose_name = 'farmer organization'
        verbose_name = 'farmer organizations'

    def __str__(self):
        return str(self.family)
