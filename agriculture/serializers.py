from rest_framework import serializers

from .models import (
    Animal,
    FarmWastePractice,
    Breed,
    Crop,
    CropType,
    FamilyCrop,
    FamilyCropInfo,
    FamilyFarm,
    FamilyLiveStock,
    FamilyPetAndPoultry,
    FarmLabor,
    FarmMachinery,
    FarmMarketing,
    FarmWorker,
    FarmerOrganization,
    FarmingPractice,
    FarmingProblem,
    FinancingPractice,
    LiveStock,
    MarketInfo,
    MarketOutlet,
    OrgThought,
    Poultry,
    ProductionEconomy,
    SeedType,
    SellingPrice,
    TenurialInstrument,
)


class AnimalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Animal
        fields = '__all__'


class BreedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Breed
        fields = '__all__'


class CropTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CropType
        fields = '__all__'


class FinancingPracticeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FinancingPractice
        fields = '__all__'


class MarketOutletSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketOutlet
        fields = '__all__'


class FarmMarketingSerializer(serializers.ModelSerializer):
    class Meta:
        model = FarmMarketing
        fields = '__all__'


class FarmWorkerSerializer(serializers.ModelSerializer):
    class Meta:
        model = FarmWorker
        fields = '__all__'


class FarmingPracticeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FarmingPractice
        fields = '__all__'


class TenurialInstrumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = TenurialInstrument
        fields = '__all__'


class FamilyCropSerializer(serializers.ModelSerializer):
    class Meta:
        model = FamilyCrop
        fields = '__all__'


class FamilyCropInfoSerializer(serializers.ModelSerializer):
    crop_type = CropTypeSerializer()
    marketing = FarmMarketingSerializer(many=True)
    market_outlets = MarketOutletSerializer(many=True)
    financing_practices = FinancingPracticeSerializer(many=True)
    crops = serializers.SerializerMethodField()

    class Meta:
        model = FamilyCropInfo
        fields = '__all__'

    def get_crops(self, obj):
        return FamilyCropSerializer(obj.familycrop_set.all(), many=True).data


class FamilyLiveStockSerializer(serializers.ModelSerializer):
    live_stocks = serializers.SerializerMethodField()

    class Meta:
        model = FamilyLiveStock
        fields = '__all__'

    def get_live_stocks(self, obj):
        return LiveStockSerializer(obj.livestock_set.all(), many=True).data


class FamilyPetAndPoultrySerializer(serializers.ModelSerializer):
    poultries = serializers.SerializerMethodField()

    class Meta:
        model = FamilyPetAndPoultry
        fields = '__all__'

    def get_poultries(self, obj):
        return PoultrySerializer(obj.poultry_set.all(), many=True).data


class FarmLaborSerializer(serializers.ModelSerializer):
    class Meta:
        model = FarmLabor
        fields = '__all__'


class FarmMachinerySerializer(serializers.ModelSerializer):
    class Meta:
        model = FarmMachinery
        fields = '__all__'


class LiveStockSerializer(serializers.ModelSerializer):
    animal = AnimalSerializer()
    breeds = BreedSerializer(many=True)

    class Meta:
        model = LiveStock
        fields = '__all__'


class MarketInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketInfo
        fields = '__all__'


class PoultrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Poultry
        fields = '__all__'


class ProductionEconomySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductionEconomy
        fields = '__all__'


class OrgThoughtSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrgThought
        fields = '__all__'


class FarmWastePracticeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FarmWastePractice
        fields = '__all__'


class FamilyFarmSerializer(serializers.ModelSerializer):
    tenurial_instrument = TenurialInstrumentSerializer(read_only=True)
    farming_practice = FarmingPracticeSerializer(read_only=True)
    waste_practices = FarmWastePracticeSerializer(many=True, read_only=True)
    workers = FarmWorkerSerializer(many=True, read_only=True)
    labors = serializers.SerializerMethodField()

    class Meta:
        model = FamilyFarm
        fields = '__all__'

    def get_labors(self, obj):
        return FarmLaborSerializer(obj.farmlabor_set.all(), many=True).data


class FarmerOrganizationSerializer(serializers.ModelSerializer):
    thoughts = OrgThoughtSerializer(many=True, read_only=True)

    class Meta:
        model = FarmerOrganization
        fields = '__all__'
