from django.conf.urls import include, url

from rest_framework.routers import DefaultRouter

from .views import (
    FamilyPondViewSet,
    FARInventoryViewSet,
)

router = DefaultRouter()
router.register(r'family-ponds', FamilyPondViewSet)
router.register(r'far-inventories', FARInventoryViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
]
