from rest_framework import serializers

from agriculture.serializers import (
    MarketOutletSerializer,
    FarmMarketingSerializer,
)
from fisheries.models import (
    FamilyPond,
    PondArea,
    Pond,
    FishCapture,
    FishingGear,
    FARInventory,
    FishAndAquatic,
)


class PondAreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = PondArea
        fields = '__all__'


class PondSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pond
        fields = '__all__'


class FishCaptureSerializer(serializers.ModelSerializer):
    class Meta:
        model = FishCapture
        fields = '__all__'


class FishingGearSerializer(serializers.ModelSerializer):
    class Meta:
        model = FishingGear
        fields = '__all__'


class FishAndAquaticSerializer(serializers.ModelSerializer):
    class Meta:
        model = FishAndAquatic
        fields = '__all__'


class FARInventorySerializer(serializers.ModelSerializer):
    marketing = FarmMarketingSerializer(many=True, read_only=True)
    market_outlets = MarketOutletSerializer(many=True, read_only=True)
    resources = serializers.SerializerMethodField()

    class Meta:
        model = FARInventory
        fields = '__all__'

    def get_resources(self, obj):
        return FishAndAquaticSerializer(obj.fishandaquatic_set.all(), many=True).data


class FamilyPondSerializer(serializers.ModelSerializer):
    pond_areas = PondAreaSerializer(many=True, read_only=True)
    fish_captures = FishCaptureSerializer(many=True, read_only=True)
    fishing_gears = FishingGearSerializer(many=True, read_only=True)
    ponds = serializers.SerializerMethodField()

    class Meta:
        model = FamilyPond
        fields = '__all__'

    def get_ponds(self, obj):
        return PondSerializer(obj.pond_set.all(), many=True).data
