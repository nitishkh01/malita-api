from django.contrib import admin


from .models import (
    Pond,
    PondArea,
    FishCapture,
    SpeciesCaptured,
    FishingGear,
    FamilyPond,
    FARInventory,
    FishAndAquatic,
)


class PondAdminInline(admin.TabularInline):
    model = Pond
    extra = 1


@admin.register(FamilyPond)
class FamilyPond(admin.ModelAdmin):
    inlines = [
        PondAdminInline,
    ]


class FishAndAquaticAdminInline(admin.TabularInline):
    model = FishAndAquatic
    extra = 1


@admin.register(FARInventory)
class FARInventoryAdmin(admin.ModelAdmin):
    inlines = [
        FishAndAquaticAdminInline,
    ]
