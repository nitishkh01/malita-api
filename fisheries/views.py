from rest_framework import viewsets

from .models import (
    FamilyPond,
    FARInventory,
)
from .serializers import (
    FamilyPondSerializer,
    FARInventorySerializer,
)


class FamilyPondViewSet(viewsets.ModelViewSet):
    queryset = FamilyPond.objects.prefetch_related(
        'pond_areas',
        'fish_captures',
        'fishing_gears',
        'pond_set',
    )
    serializer_class = FamilyPondSerializer


class FARInventoryViewSet(viewsets.ModelViewSet):
    queryset = FARInventory.objects.prefetch_related(
        'marketing',
        'market_outlets',
        'fishandaquatic_set',
    )
    serializer_class = FARInventorySerializer
