from django.apps import AppConfig


class FisheriesConfig(AppConfig):
    name = 'fisheries'
