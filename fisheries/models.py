from django.db import models


class Pond(models.Model):
    info = models.ForeignKey('FamilyPond', on_delete=models.CASCADE)
    area_developed = models.CharField(max_length=100, blank=True)
    cultured_species = models.CharField(max_length=100, blank=True)
    total_harvest = models.FloatField(blank=True, null=True)
    volume_sold = models.FloatField(blank=True, null=True)
    total_income = models.FloatField(blank=True, null=True)
    total_expenses = models.FloatField(blank=True, null=True)

    class Meta:
        verbose_name = 'pond'
        verbose_name_plural = 'ponds'

    def __str__(self):
        return str(self.info)


class PondArea(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'pond area'
        verbose_name_plural = 'pond areas'

    def __str__(self):
        return self.name


class FishCapture(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'fish capture'
        verbose_name_plural = 'fish captures'

    def __str__(self):
        return self.name


class SpeciesCaptured(models.Model):
    family = models.ForeignKey('families.Family', on_delete=models.CASCADE)
    species = models.CharField(max_length=200, blank=True)
    volume = models.FloatField(blank=True, null=True)
    income = models.FloatField(blank=True, null=True)
    expenses = models.FloatField(blank=True, null=True)

    class Meta:
        verbose_name = 'species captured'
        verbose_name_plural = 'species captured'

    def __str__(self):
        return str(self.family)


class FishingGear(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'fishing gear'
        verbose_name_plural = 'fishing gears'

    def __str__(self):
        return self.name


class FamilyPond(models.Model):
    VESSEL_MOTORIZED = 'motorized'
    VESSEL_NON_MOTORIZED = 'non-motorized'
    VESSEL_NO_BANCA = 'no-banca'
    VESSEL_CHOICES = (
        (VESSEL_MOTORIZED, 'Motorized'),
        (VESSEL_NON_MOTORIZED, 'Non-Motorized'),
        (VESSEL_NO_BANCA, 'No Banca'),
    )
    family = models.OneToOneField('families.Family', on_delete=models.CASCADE)
    pond_areas = models.ManyToManyField('PondArea')
    fish_captures = models.ManyToManyField('FishCapture')
    vessel = models.CharField(max_length=100, blank=True, default='', choices=VESSEL_CHOICES)
    vessel_notes = models.TextField(blank=True)
    is_vessel_registered = models.NullBooleanField(blank=True, null=True)
    registration_notes = models.TextField(blank=True)
    fishing_gears = models.ManyToManyField('FishingGear')
    other_fishing_gears = models.CharField(max_length=100, blank=True)
    fish_net_size = models.CharField(max_length=100, blank=True)
    fad_used = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = 'family pond'
        verbose_name_plural = 'family ponds'

    def __str__(self):
        return str(self.family)


class FishAndAquatic(models.Model):
    TYPE_SEAWEED = 'seaweed'
    TYPE_OYSTER_CLAM = 'oyster-or-clam'
    TYPE_CHOICES = (
        (TYPE_SEAWEED, 'Seaweed'),
        (TYPE_OYSTER_CLAM, 'Oyster / Clam'),
    )
    info = models.ForeignKey('FARInventory', on_delete=models.CASCADE)
    type = models.CharField(max_length=100, blank=True, default='', choices=TYPE_CHOICES)
    species = models.CharField(max_length=100, blank=True)
    hectares = models.FloatField(blank=True, null=True)
    average_yield = models.FloatField(blank=True, null=True)
    fresh_sold = models.FloatField(blank=True, null=True)
    dried_sold = models.FloatField(blank=True, null=True)
    fresh_income = models.FloatField(blank=True, null=True)
    dried_income = models.FloatField(blank=True, null=True)

    class Meta:
        verbose_name = 'Fish and Aquactic'
        verbose_name_plural = 'Fish and Aquactic'

    def __str__(self):
        return str(self.info)


class FARInventory(models.Model):
    family = models.ForeignKey('families.Family', on_delete=models.CASCADE)
    marketing = models.ManyToManyField('agriculture.FarmMarketing')
    market_outlets = models.ManyToManyField('agriculture.MarketOutlet')
    other_marketing = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = 'FAR Inventory'
        verbose_name_plural = 'FAR Inventories'

    def __str__(self):
        return str(self.family)
