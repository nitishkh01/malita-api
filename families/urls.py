from django.conf.urls import include, url

from rest_framework.routers import DefaultRouter

from .views import (
    BarangayViewSet,
    ConstantView,
    HeadViewSet,
    SpouseViewSet,
    FamilyViewSet,
    FamilyProfessionViewSet,
    PsychoSocialViewSet,
)

router = DefaultRouter()
router.register(r'barangays', BarangayViewSet)
router.register(r'dependents', HeadViewSet)
router.register(r'heads', HeadViewSet)
router.register(r'professions', FamilyProfessionViewSet)
router.register(r'psychosocial', PsychoSocialViewSet)
router.register(r'spouses', SpouseViewSet)
router.register(r'', FamilyViewSet)


urlpatterns = [
    url(r'^constants/$', ConstantView.as_view()),
    url(r'^', include(router.urls)),
]
