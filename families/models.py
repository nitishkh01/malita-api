from django.db import models

from core.upload_path import get_images_upload_path
from .constants import (
    BLOOD_TYPE_CHOICES,
    CIVIL_STATUS_CHOICES,
    EDUCATIONAL_ATTAINMENT_CHOICES,
    GENDER_CHOICES,
    RELIGION_CHOICES,
)


class BaseProfileInfo(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to=get_images_upload_path, blank=True)
    contact_number = models.CharField(max_length=100, blank=True)
    religion = models.CharField(max_length=20, choices=RELIGION_CHOICES, blank=True)
    religion_others = models.CharField(max_length=100, blank=True)
    birth_date = models.DateField(blank=True, null=True)
    blood_type = models.CharField(max_length=9, blank=True, choices=BLOOD_TYPE_CHOICES, default='')
    highest_educational_attainment = models.CharField(
        max_length=100, blank=True, choices=EDUCATIONAL_ATTAINMENT_CHOICES, default=''
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class LGU(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'LGU'
        verbose_name_plural = 'LGUs'

    def __str__(self):
        return self.name


class Barangay(models.Model):
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.name


class Head(BaseProfileInfo):
    gender = models.CharField(max_length=100, blank=True, choices=GENDER_CHOICES, default='')
    civil_status = models.CharField(max_length=100, blank=True, choices=CIVIL_STATUS_CHOICES, default='')
    cellphone_number = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = 'head'
        verbose_name_plural = 'heads'


class Spouse(BaseProfileInfo):
    head = models.ForeignKey('Head', related_name='spouses', blank=True, null=True)

    class Meta:
        verbose_name = 'spouse'
        verbose_name_plural = 'spouse'


class Dependent(models.Model):
    name = models.CharField(max_length=50)
    birth_date = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=100, blank=True, choices=GENDER_CHOICES, default='')
    relation = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = 'dependent'
        verbose_name_plural = 'dependents'


class FamilyQuerySet(models.QuerySet):

    def search(self, query):
        return self.filter(
            models.Q(address__icontains=query) |
            models.Q(dependents_text__icontains=query) |
            models.Q(head__civil_status__icontains=query) |
            models.Q(head__gender__icontains=query) |
            models.Q(head__blood_type__icontains=query) |
            models.Q(government_aid_received__icontains=query) |
            models.Q(head__birth_date__icontains=query) |
            models.Q(housing__vulnerability_and_risks__name__icontains=query) |
            models.Q(familyincome__gross_monthly_income__value__icontains=query) |
            models.Q(head__name__icontains=query) |
            models.Q(lgu__name__icontains=query) |
            models.Q(spouse__name__icontains=query) |
            models.Q(tracking_number__icontains=query) |
            models.Q(head__religion__icontains=query) |
            models.Q(head__highest_educational_attainment__icontains=query) |
            models.Q(tribe_name__icontains=query)
        ).distinct()

    def without_barangays(self, **kwargs):
        return self.filter(
            barangay__isnull=True, **kwargs)

    def with_barangays(self, **kwargs):
        return self.filter(
            barangay__isnull=False, **kwargs)

    def without_government_aid(self, **kwargs):
        return self.filter(
            government_aid_received__isnull=True, **kwargs)

    def with_government_aid(self, **kwargs):
        return self.filter(
            government_aid_received__isnull=False, **kwargs)

    def without_highest_educational_attainment(self, **kwargs):
        return self.filter(
            head__highest_educational_attainment__isnull=True, **kwargs)

    def with_highest_educational_attainment(self, **kwargs):
        return self.filter(
            head__highest_educational_attainment__isnull=False, **kwargs)

    def without_housing_risk(self, **kwargs):
        return self.filter(
            housing__vulnerability_and_risks__isnull=True, **kwargs)

    def with_housing_risk(self, **kwargs):
        return self.filter(
            housing__vulnerability_and_risks__isnull=False, **kwargs)

    def without_income(self, **kwargs):
        return self.filter(
            familyincome__gross_monthly_income__isnull=True, **kwargs)

    def with_income(self, **kwargs):
        return self.filter(
            familyincome__gross_monthly_income__isnull=False, **kwargs)

    def without_gender(self, **kwargs):
        return self.filter(
            head__gender__isnull=True, **kwargs)

    def with_gender(self, **kwargs):
        return self.filter(
            head__gender__isnull=False, **kwargs)


class Family(models.Model):
    TRIBE_TYPE_IP = 'ip'
    TRIBE_TYPE_NON_IP = 'non-ip'
    TRIBE_TYPE_MUSLIM = 'muslim'
    TRIBE_TYPE_CHOICES = (
        (TRIBE_TYPE_IP, 'IP'),
        (TRIBE_TYPE_NON_IP, 'Non-IP'),
        (TRIBE_TYPE_MUSLIM, 'Muslim'),
    )

    AID_RECEIVED_NHTS = 'nhts'
    AID_RECEIVED_NON_4PS = 'non-4ps'
    AID_RECEIVED_4PS = '4ps'
    AID_RECEIVED_CHOICES = (
        (AID_RECEIVED_NHTS, 'NHTS'),
        (AID_RECEIVED_NON_4PS, 'Non-4Ps'),
        (AID_RECEIVED_4PS, '4Ps'),
    )

    wordpress_db = models.CharField(max_length=5, blank=True)
    wordpress_id = models.PositiveSmallIntegerField(blank=True, null=True)
    head = models.ForeignKey('Head', on_delete=models.CASCADE, related_name='family')
    spouse = models.ForeignKey('Spouse', on_delete=models.CASCADE, blank=True, null=True, related_name='family')
    lgu = models.ForeignKey('LGU', on_delete=models.CASCADE)
    tracking_number = models.CharField(max_length=100, blank=True)
    series_number = models.CharField(max_length=100, blank=True)
    address = models.CharField(max_length=200, blank=True)
    barangay = models.ForeignKey('Barangay', blank=True, null=True)
    num_of_male_children = models.PositiveSmallIntegerField(blank=True, null=True)
    num_of_female_children = models.PositiveSmallIntegerField(blank=True, null=True)
    tribe_type = models.CharField(max_length=100, blank=True, default='', choices=TRIBE_TYPE_CHOICES)
    tribe_name = models.CharField(max_length=100, blank=True)
    government_aid_received = models.CharField(
        max_length=100, blank=True, choices=AID_RECEIVED_CHOICES, default=''
    )
    dependents = models.ManyToManyField('Dependent', blank=True)
    dependents_text = models.TextField(blank=True)
    map_points = models.CharField(max_length=100, help_text='3 Points separated by comma.', blank=True)

    objects = FamilyQuerySet.as_manager()

    class Meta:
        verbose_name = 'family'
        verbose_name_plural = 'families'

    def __str__(self):
        return str(self.head)


class FamilyProfession(models.Model):
    family = models.ForeignKey('Family', on_delete=models.CASCADE)
    name = models.CharField(max_length=100, blank=True)
    age = models.CharField(max_length=100, blank=True)
    type_of_skill = models.CharField(max_length=100, blank=True)
    degree = models.CharField(max_length=100, blank=True)
    skills_acquired_through = models.CharField(max_length=100, blank=True)
    remarks = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = 'Profession and Skills'
        verbose_name_plural = 'Profession and Skills'

    def __str__(self):
        return self.name


class ServiceGrant(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'service'
        verbose_name_plural = 'services'

    def __str__(self):
        return self.name


class Disability(models.Model):
    family = models.ForeignKey('Family', on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    number = models.PositiveSmallIntegerField(blank=True, null=True)

    class Meta:
        verbose_name = 'disability'
        verbose_name_plural = 'disabilities'

    def __str__(self):
        return self.name


class PsychoSocial(models.Model):
    HELP_VERY_MUCH = 'very-much'
    HELP_MUCH = 'much'
    HELP_NOT_SO_MUCH = 'not so much'
    HELP_LITTLE = 'little'
    HELP_CHOICES = (
        (HELP_VERY_MUCH, 'Very Much'),
        (HELP_MUCH, 'Much'),
        (HELP_NOT_SO_MUCH, 'Not so much'),
        (HELP_LITTLE, 'Little'),
    )
    family = models.OneToOneField('Family', on_delete=models.CASCADE)
    violence_against_women = models.NullBooleanField(blank=True, null=True)
    num_of_violence_against_women = models.CharField(max_length=100, blank=True)
    violence_against_children = models.NullBooleanField(blank=True, null=True)
    num_of_violence_against_children = models.CharField(max_length=100, blank=True)
    conflict_with_law = models.NullBooleanField(blank=True, null=True)
    victims_of_armed_conflict = models.NullBooleanField(blank=True, null=True)
    practice_family_planning = models.NullBooleanField(blank=True, null=True)
    family_planning_method = models.CharField(max_length=100, blank=True)
    availed_services = models.ManyToManyField('ServiceGrant', blank=True)
    is_national_gov_helping = models.CharField(max_length=15, blank=True, default='', choices=HELP_CHOICES)
    is_provincial_gov_helping = models.CharField(max_length=15, blank=True, default='', choices=HELP_CHOICES)
    is_municipal_gov_helping = models.CharField(max_length=15, blank=True, default='', choices=HELP_CHOICES)
    is_barangay_gov_helping = models.CharField(max_length=15, blank=True, default='', choices=HELP_CHOICES)
    name_of_respondent = models.CharField(max_length=100, blank=True)
    name_of_enumerator = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = 'psycho-social care'
        verbose_name_plural = 'psycho-social cares'

    def __str__(self):
        return str(self.family)
