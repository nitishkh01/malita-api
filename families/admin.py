from django.contrib import admin

from .models import (
    Family,
    FamilyProfession,
    Head,
    PsychoSocial,
    Spouse,
    Disability,
)


@admin.register(Family)
class FamilyAdmin(admin.ModelAdmin):
    list_display = [
        'head',
        'address',
    ]
    list_filter = [
        'barangay',
    ]
    raw_id_fields = [
        'head',
        'spouse',
    ]
    search_fields = [
        'head__name',
        'spouse__head',
    ]


admin.site.register(FamilyProfession)
admin.site.register(Head)
admin.site.register(Spouse)
admin.site.register(PsychoSocial)
admin.site.register(Disability)
