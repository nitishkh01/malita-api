# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-02-26 06:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('families', '0016_add_head_in_spouse'),
    ]

    operations = [
        migrations.AlterField(
            model_name='family',
            name='head',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='family', to='families.Head'),
        ),
        migrations.AlterField(
            model_name='family',
            name='spouse',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='family', to='families.Spouse'),
        ),
    ]
