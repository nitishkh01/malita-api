# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-01-25 17:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('families', '0011_fix-religion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='family',
            name='dependents',
            field=models.ManyToManyField(blank=True, to='families.Dependent'),
        ),
        migrations.AlterField(
            model_name='head',
            name='religion',
            field=models.CharField(blank=True, choices=[('roman-catholic', 'Roman Catholic'), ('uccp', 'UCCP'), ('inc', 'INC'), ('baptist', 'Baptist'), ('islam', 'Islam'), ('others', 'Others')], max_length=20),
        ),
        migrations.AlterField(
            model_name='spouse',
            name='religion',
            field=models.CharField(blank=True, choices=[('roman-catholic', 'Roman Catholic'), ('uccp', 'UCCP'), ('inc', 'INC'), ('baptist', 'Baptist'), ('islam', 'Islam'), ('others', 'Others')], max_length=20),
        ),
    ]
