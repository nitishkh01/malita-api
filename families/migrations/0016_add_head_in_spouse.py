# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-02-26 06:38
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('families', '0015_auto_20180219_0208'),
    ]

    operations = [
        migrations.AddField(
            model_name='spouse',
            name='head',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='spouses', to='families.Head'),
        ),
    ]
