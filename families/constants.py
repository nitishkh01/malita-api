GENDER_MALE = 'male'
GENDER_FEMALE = 'female'
GENDER_CHOICES = (
    (GENDER_MALE, 'Male'),
    (GENDER_FEMALE, 'Female'),
)

CIVIL_STATUS_SINGLE = 'single'
CIVIL_STATUS_MARRIED = 'married'
CIVIL_STATUS_SEPARATED = 'separated'
CIVIL_STATUS_WIDOWED = 'widowed'
CIVIL_STATUS_ANNULLED = 'annulled'
CIVIL_STATUS_OTHERS = 'others'
CIVIL_STATUS_CHOICES = (
    (CIVIL_STATUS_SINGLE, 'Single'),
    (CIVIL_STATUS_MARRIED, 'Married'),
    (CIVIL_STATUS_SEPARATED, 'Separated'),
    (CIVIL_STATUS_WIDOWED, 'Widowed'),
    (CIVIL_STATUS_ANNULLED, 'Annulled'),
    (CIVIL_STATUS_OTHERS, 'Others'),
)

RELIGION_ROMAN_CATHOLIC = 'roman-catholic'
RELIGION_UCCP = 'uccp'
RELIGION_INC = 'inc'
RELIGION_BAPTIST = 'baptist'
RELIGION_ISLAM = 'islam'
RELIGION_OTHERS = 'others'
RELIGION_CHOICES = (
    (RELIGION_ROMAN_CATHOLIC, 'Roman Catholic'),
    (RELIGION_UCCP, 'UCCP'),
    (RELIGION_INC, 'INC'),
    (RELIGION_BAPTIST, 'Baptist'),
    (RELIGION_ISLAM, 'Islam'),
    (RELIGION_OTHERS, 'Others'),
)

BLOOD_TYPE_A = 'a'
BLOOD_TYPE_AB = 'ab'
BLOOD_TYPE_B = 'b'
BLOOD_TYPE_O = 'o'
BLOOD_TYPE_UNDEFINED = 'undefined'
BLOOD_TYPE_CHOICES = (
    (BLOOD_TYPE_A, 'A'),
    (BLOOD_TYPE_AB, 'AB'),
    (BLOOD_TYPE_B, 'B'),
    (BLOOD_TYPE_O, 'O'),
    (BLOOD_TYPE_UNDEFINED, 'Undefined'),
)

EDUCATIONAL_ATTAINMENT_ELEMENTARY = 'elementary'
EDUCATIONAL_ATTAINMENT_HIGH_SCHOOL = 'high-school'
EDUCATIONAL_ATTAINMENT_COLLEGE = 'college'
EDUCATIONAL_ATTAINMENT_POST_GRADUATE = 'post-graduate'
EDUCATIONAL_ATTAINMENT_NONE = 'none'
EDUCATIONAL_ATTAINMENT_CHOICES = (
    (EDUCATIONAL_ATTAINMENT_ELEMENTARY, 'Elementary'),
    (EDUCATIONAL_ATTAINMENT_HIGH_SCHOOL, 'High School'),
    (EDUCATIONAL_ATTAINMENT_COLLEGE, 'College'),
    (EDUCATIONAL_ATTAINMENT_POST_GRADUATE, 'Post-Graduate'),
)


PHILHEALTH_MLGU = 'mlgu-provided'
PHILHEALTH_PLGU = 'plgu-provided'
PHILHEALTH_INDIVIDUAL = 'individual-paying'
PHILHEALTH_EMPLOYER = 'employer'
PHILHEALTH_4PS = '4ps'
PHILHEALTH_NONE = 'none'
PHILHEALTH_CHOICES = (
    (PHILHEALTH_MLGU, 'MLGU Provided'),
    (PHILHEALTH_PLGU, 'PLGU Provided'),
    (PHILHEALTH_INDIVIDUAL, 'Individually Paying'),
    (PHILHEALTH_EMPLOYER, 'Employer'),
    (PHILHEALTH_4PS, '4Ps'),
    (PHILHEALTH_NONE, 'None'),
)


YES = 'yes'
NO = 'no'
YES_NO_CHOICES = (
    (YES, 'Yes'),
    (NO, 'No'),
)


NULL_BOOLEAN_CHOICES = (
    (2, 'Yes'),
    (3, 'No'),
)


OWNERSHIP_OWNED = 'owned'
OWNERSHIP_RENTED = 'rented'
OWNERSHIP_MORTGAGED = 'mortgaged'
OWNERSHIP_SQUATTER = 'squatter'
OWNERSHIP_INFORMAL_SETTLER = 'informal-settler'
OWNERSHIP_CHOICES = (
    (OWNERSHIP_OWNED, 'Owned'),
    (OWNERSHIP_RENTED, 'Rented'),
    (OWNERSHIP_MORTGAGED, 'Mortgaged'),
    (OWNERSHIP_SQUATTER, 'Squatter'),
    (OWNERSHIP_INFORMAL_SETTLER, 'Informal Settler'),
)

HOME_RURAL_COMMUNITY = 'rural-community'
HOME_VILLAGE_SITIO = 'village-sitio'
HOME_SEMI_URBAN = 'semi-urban'
HOME_URBAN = 'urban'
HOME_CHOICES = (
    (HOME_RURAL_COMMUNITY, 'Rural Community'),
    (HOME_VILLAGE_SITIO, 'Village / Sitio'),
    (HOME_SEMI_URBAN, 'Semi-Urban'),
    (HOME_URBAN, 'Urban'),
)

FREQ_NONE = 'none'
FREQ_ONCE = 'once'
FREQ_TWICE = 'twice'
FREQ_THRICE = 'thrice'
FREQ_FOUR_OR_MORE = 'four-or-more'
FREQ_CHOICES = (
    (FREQ_NONE, 'None'),
    (FREQ_ONCE, 'Once'),
    (FREQ_TWICE, 'Twice'),
    (FREQ_THRICE, 'Thrice'),
    (FREQ_FOUR_OR_MORE, 'Four or More Times'),
)

DRAINAGE_OPEN_CANAL = 'open-canal'
DRAINAGE_BLIND_TYPE = 'blind-type'
DRAINAGE_NONE = 'none'
DRAINAGE_CHOICES = (
    (DRAINAGE_OPEN_CANAL, 'Open Canal Type'),
    (DRAINAGE_BLIND_TYPE, 'Blind Type'),
    (DRAINAGE_NONE, 'None'),
)

FARM_TYPE_RAINFED = 'rainfed'
FARM_TYPE_IRRIGATED = 'irrigated'
FARM_TYPE_CHOICES = (
    (FARM_TYPE_RAINFED, 'Rainfed'),
    (FARM_TYPE_IRRIGATED, 'Irrigated'),
)


MEETING_SELDOM = 'seldom'
MEETING_OFTEN = 'often'
MEETING_NONE = 'none-at-all'
MEETING_CHOICES = (
    (MEETING_SELDOM, 'Seldom'),
    (MEETING_OFTEN, 'Often'),
    (MEETING_NONE, 'None at all'),
)

FREQ_ONCE_A_WEEK = 'once-a-week'
FREQ_ONCE_IN_TWO_WEEKS = 'once-in-two-weeks'
FREQ_ONCE_A_MONTH = 'once-a-month'
FREQ_LESSER = 'lesser'
FREQ_CHOICES = (
    (FREQ_ONCE_A_WEEK, 'Once a week'),
    (FREQ_ONCE_IN_TWO_WEEKS, 'Once in 2 weeks'),
    (FREQ_ONCE_A_MONTH, 'Once a month'),
    (FREQ_LESSER, 'Less than 1 in 2 months'),
)

USEFUL_VERY_MUCH = 'very-much'
USEFUL_MUCH = 'much'
USEFUL_MEDIUM = 'medium'
USEFUL_NOT_SO_MUCH = 'not-so-much'
USEFUL_GENERALLY_NOT = 'generally-not'
USEFUL_CHOICES = (
    (USEFUL_VERY_MUCH, 'very much'),
    (USEFUL_MUCH, 'much'),
    (USEFUL_MEDIUM, 'medium'),
    (USEFUL_NOT_SO_MUCH, 'not so much / sometimes'),
    (USEFUL_GENERALLY_NOT, 'generally, not'),
)
