from income.serializers import FamilyIncomeSerializer

from nutrition.serializers import NutritionProfileSerializer

from housing.serializers import FamilyHousingSerializer

from agriculture.serializers import (
    FamilyFarmSerializer,
    FamilyCropInfoSerializer,
    FarmerOrganizationSerializer
)

from rest_framework import serializers

from .models import (
    Barangay,
    Family,
    FamilyProfession,
    Head,
    LGU,
    PsychoSocial,
    Spouse,
    Dependent
)


class LGUSerializer(serializers.ModelSerializer):
    class Meta:
        model = LGU
        fields = '__all__'


class BarangaySerializer(serializers.ModelSerializer):
    class Meta:
        model = Barangay
        fields = '__all__'


class HeadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Head
        fields = '__all__'


class SpouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Spouse
        fields = '__all__'


class DependentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dependent
        fields = '__all__'


class FamilySerializer(serializers.ModelSerializer):
    head = HeadSerializer(read_only=True)
    spouse = SpouseSerializer(read_only=True)
    dependents = DependentSerializer(read_only=True)
    lgu = LGUSerializer(read_only=True)
    income = FamilyIncomeSerializer(source='familyincome', read_only=True)
    barangay = BarangaySerializer(read_only=True)
    nutrition = NutritionProfileSerializer(read_only=True)
    housing = FamilyHousingSerializer(read_only=True)
    family_farm = FamilyFarmSerializer(read_only=True)
    family_crop = FamilyCropInfoSerializer(read_only=True)
    family_organization = FarmerOrganizationSerializer(read_only=True)
    gps_lat = serializers.DecimalField(
        max_digits=9, decimal_places=6, source='housing.gps_lat', read_only=True)
    gps_lon = serializers.DecimalField(
        max_digits=9, decimal_places=6, source='housing.gps_lon', read_only=True)

    class Meta:
        model = Family
        fields = '__all__'


class FamilyWriteSerializer(FamilySerializer):
    class Meta:
        model = Family
        fields = '__all__'


class FamilyProfessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = FamilyProfession
        fields = '__all__'


class PsychosocialSerializer(serializers.ModelSerializer):
    class Meta:
        model = PsychoSocial
        fields = '__all__'
