import rest_framework_filters as filters

from .models import Family


class FamilyFilter(filters.FilterSet):
    search = filters.CharFilter(method='search_filter')

    class Meta:
        model = Family
        fields = {
            'barangay': ['exact', 'in'],
            'crops__crop_type': ['exact', 'in'],
            'farm__pakyaw_or_labor': ['exact', 'in'],
            'farmer_org__meeting': ['exact', 'in'],
            'farmer_org__meeting_frequency': ['exact', 'in'],
            'farmer_org__usefullness': ['exact', 'in'],
            'head__blood_type': ['exact', 'in'],
            'head__civil_status': ['exact', 'in'],
            'head__gender': ['exact', 'in'],
            'head__highest_educational_attainment': ['exact', 'in'],
            'head__religion': ['exact', 'in'],
            'housing__earthquake_frequency': ['exact', 'in'],
            'housing__flood_frequency': ['exact', 'in'],
            'housing__home_settings': ['exact', 'in'],
            'housing__landslide_frequency': ['exact', 'in'],
            'housing__ownership_status': ['exact', 'in'],
            'housing__tornado_frequency': ['exact', 'in'],
            'nutrition__has_children_immunized': ['exact'],
            'nutrition__has_death_below_1_year_old': ['exact'],
            'nutrition__has_malnourished_children': ['exact'],
            'nutrition__has_newborn_estimated_2_5': ['exact'],
            'nutrition__has_pregnancy_supplement': ['exact'],
            'nutrition__has_prenatal_services': ['exact'],
            'nutrition__has_trained_delivery': ['exact'],
            'nutrition__philhealth_membership': ['exact', 'in'],
            'spouse__blood_type': ['exact', 'in'],
            'spouse__highest_educational_attainment': ['exact', 'in'],
            'spouse__religion': ['exact', 'in'],
        }

    def search_filter(self, qs, name, value):
        return qs.search(value)
