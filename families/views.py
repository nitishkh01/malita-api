import base64

from django.core.files.base import ContentFile

from rest_framework import (
    views,
    viewsets,
)
from rest_framework.decorators import (
    detail_route,
)
from rest_framework.response import Response

from core.exports import get_excel_response
from core.views.mixins import ValidateMixin
from . import constants
from .filters import FamilyFilter
from .models import (
    Barangay,
    Dependent,
    Family,
    FamilyProfession,
    Head,
    PsychoSocial,
    Spouse,
)

from .serializers import (
    BarangaySerializer,
    DependentSerializer,
    FamilySerializer,
    FamilyWriteSerializer,
    HeadSerializer,
    SpouseSerializer,
    FamilyProfessionSerializer,
    PsychosocialSerializer,
)


class BarangayViewSet(viewsets.ModelViewSet):
    queryset = Barangay.objects.all()
    serializer_class = BarangaySerializer


class ConstantView(views.APIView):

    def _format_choices(self, choices):
        return [{'value': item[0], 'label': item[1], 'text': item[1]} for item in choices]

    def get(self, request, format=None):
        data = {
            'blood_type': self._format_choices(constants.BLOOD_TYPE_CHOICES),
            'civil_status': self._format_choices(constants.CIVIL_STATUS_CHOICES),
            'educational_attainment': self._format_choices(constants.EDUCATIONAL_ATTAINMENT_CHOICES),
            'farm_type': self._format_choices(constants.FARM_TYPE_CHOICES),
            'frequencies': self._format_choices(constants.FREQ_CHOICES),
            'gender': self._format_choices(constants.GENDER_CHOICES),
            'government_aid_received': self._format_choices(Family.AID_RECEIVED_CHOICES),
            'home_setting': self._format_choices(constants.HOME_CHOICES),
            'meeting_choices': self._format_choices(constants.MEETING_CHOICES),
            'meeting_frequency': self._format_choices(constants.FREQ_CHOICES),
            'null_boolean': self._format_choices(constants.NULL_BOOLEAN_CHOICES),
            'ownership_status': self._format_choices(constants.OWNERSHIP_CHOICES),
            'philhealth_membership': self._format_choices(constants.PHILHEALTH_CHOICES),
            'religion': self._format_choices(constants.RELIGION_CHOICES),
            'tribe_type': self._format_choices(Family.TRIBE_TYPE_CHOICES),
            'usefullness': self._format_choices(constants.USEFUL_CHOICES),
            'yes_no': self._format_choices(constants.YES_NO_CHOICES),
        }
        return Response(data)


class FamilyViewSet(ValidateMixin, viewsets.ModelViewSet):
    queryset = Family.objects.select_related(
        'barangay',
        'farm',
        'farmer_org',
        'head',
        'housing',
        'lgu',
        'nutrition',
        'spouse',
    ).prefetch_related('crops')
    serializer_class = FamilySerializer
    filter_class = FamilyFilter

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return FamilySerializer
        else:
            return FamilyWriteSerializer

    def list(self, request):
        queryset = self.filter_queryset(self.get_queryset())

        if request.query_params.get('export'):
            return get_excel_response(queryset)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(self.add_rank(serializer.data))


class HeadViewSet(ValidateMixin, viewsets.ModelViewSet):
    queryset = Head.objects.all()
    serializer_class = HeadSerializer

    @detail_route(methods=['post', 'patch'], url_path='upload-image')
    def upload_image(self, request, *args, **kwargs):
        head = self.get_object()
        img_str = request.data['data_url'].split(';base64,')[1]
        head.image.save(request.data['file_name'], ContentFile(base64.b64decode(img_str)))
        return Response({'image': head.image.url})


class SpouseViewSet(ValidateMixin, viewsets.ModelViewSet):
    queryset = Spouse.objects.all()
    serializer_class = SpouseSerializer


class FamilyProfessionViewSet(viewsets.ModelViewSet):
    queryset = FamilyProfession.objects.all()
    serializer_class = FamilyProfessionSerializer


class PsychoSocialViewSet(viewsets.ModelViewSet):
    queryset = PsychoSocial.objects.all()
    serializer_class = PsychosocialSerializer


class DependentViewSet(ValidateMixin, viewsets.ModelViewSet):
    queryset = Dependent.objects.all()
    serializer_class = DependentSerializer
