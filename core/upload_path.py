import os


def get_images_upload_path(instance, filename):
    return os.path.join(
        'images',
        instance.__class__.__name__.lower(),
        str(instance.id),
        filename
    )
