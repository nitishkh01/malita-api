import tempfile

import xlsxwriter

from django.db import models
from django.http import StreamingHttpResponse
from django.utils import timezone


def annotate_null_boolean_fields(field_names):
    """
    Django Docs: https://docs.djangoproject.com/en/2.0/topics/db/aggregation/

    Converts value of NullBooleanField from TRUE/FALSE to Yes/No
    When using this, you need to add a prefix in the new field `str_`,
    For example, if the field name is `education__has_children_in_elementary',
    When using it in `values_list`, you need to add `str_`.

    queryset.values_list('str_education__has_children_in_elementary')

    :param field_names:
        list -- List of field names
    :returns:
        dictionary -- dictionary of annotation
    """
    query = {}

    for field_name in field_names:
        yes_query = {
            field_name: True,
            'then': models.Value('Yes')
        }
        no_query = {
            field_name: False,
            'then': models.Value('No')
        }
        query.update({
            'str_{}'.format(field_name): models.Case(
                models.When(
                    **yes_query
                ),
                models.When(
                    **no_query
                ),
                output_field=models.CharField()
            )
        })

    return query


def get_basic_info_data(queryset):
    data = [(
        'Family Head',
        'LGU',
        'Address',
        'Barangay',
        'Birth Date',
        'Gender',
        'Civil Status',
        'Spouse',
        'No. of Male Children',
        'No. of Female Children',
        'Tribe Type',
        'Tribe Name',
        'Government Aid Received',
        'Religion',
        'Highest Educ. Attainment',
        'Monthly Income',
        'Other src of Income',
    )]
    data.extend(
        queryset.values_list(
            'head__name',
            'lgu__name',
            'address',
            'barangay__name',
            'head__birth_date',
            'head__gender',
            'head__civil_status',
            'spouse__name',
            'num_of_male_children',
            'num_of_female_children',
            'tribe_type',
            'tribe_name',
            'government_aid_received',
            'head__religion',
            'head__highest_educational_attainment',
            'familyincome__gross_monthly_income',
            'familyincome__other_income_sources',
        )
    )
    return data


def get_education_data(queryset):
    data = [(
        'Family Head',
        'Has children 6-12 years old in Elementary School',
        'Has family members (10 years and above) able to read and write, and do simple calculations',
        'Has family members able to vote at elections',
        'Has family members able to attend barangay assembly, PTCA meetings, Purok Meetings, etc',
        'No. of children (2-4 years old) in Day Care',
        'No. of children (5-year old) in kindergarten',
        'No. of children (6-12 years old) not in elementary',
        'No. of children not in Junior High School',
        'No. of children not in Senior High School',
        'No. of children not in College',
        'No. of children who Graduated in College',
        'No. of children at College Level',
        'No. of children who are High School graduate',
        'No. of children who are High School Level',
        'No. of children who are Elementary Graduate',
        'No. of children who are Elementary Level',
    )]
    yes_no_fields = [
        'education__has_children_in_elementary',
        'education__has_members_able_to_read_and_write',
        'education__has_members_able_to_vote',
        'education__has_members_able_to_attend_barangay_assembly',
    ]
    queryset = queryset.annotate(
        **annotate_null_boolean_fields(yes_no_fields)
    )
    data.extend(
        queryset.values_list(
            'head__name',

            # We add `str_` here since these are fields from `annotate_null_boolean_fields`
            'str_education__has_children_in_elementary',
            'str_education__has_members_able_to_read_and_write',
            'str_education__has_members_able_to_vote',
            'str_education__has_members_able_to_attend_barangay_assembly',

            'education__children_in_daycare',
            'education__children_in_kinder',
            'education__children_not_in_elementary',
            'education__children_not_in_junior_hs',
            'education__children_not_in_senior_hs',
            'education__children_not_in_college',
            'education__children_college_graduate',
            'education__children_college_level',
            'education__children_high_school_graduate',
            'education__children_high_school_level',
            'education__children_elementary_graduate',
            'education__children_elementary_level',
        )
    )
    return data


def create_excel_file(queryset, **options):
    temp_file = tempfile.TemporaryFile()

    if 'constant_memory' not in options:
        options.update({
            'constant_memory': True
        })

    queryset = queryset.order_by('head__name')

    workbook = xlsxwriter.Workbook(temp_file, options)
    basic_info_worksheet = workbook.add_worksheet('Basic Information')
    education_worksheet = workbook.add_worksheet('Education')

    for idx, row in enumerate(get_basic_info_data(queryset)):
        basic_info_worksheet.write_row(idx, 0, row)

    for idx, row in enumerate(get_education_data(queryset)):
        education_worksheet.write_row(idx, 0, row)

    workbook.close()
    temp_file.seek(0)
    return temp_file


def get_excel_response(queryset, **options):
    temp_file = create_excel_file(queryset)
    file_name = 'families_{}.xlsx'.format(timezone.now().strftime('%Y_%m_%d'))
    response = StreamingHttpResponse(temp_file, content_type="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename={}'.format(file_name)
    return response
