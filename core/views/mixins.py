from rest_framework import (
    response,
    status,
)
from rest_framework.decorators import list_route


class PaginatedResponseMixin(object):
    """
    Adds utility method for getting paginated response, usually used in
    custom list and detail_route that returns list
    """

    def paginated_response(self, queryset, serializer_class=None):
        if serializer_class is None:
            serializer_class = self.get_serializer_class()

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = serializer_class(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = serializer_class(queryset, many=True)
        return response.Response(serializer.data)


class ValidateMixin(object):
    """
    Adds /validate/ list_route for validating data
    """

    def _validate_list(self, data):
        errors = {}
        for item in data:
            serializer = self.get_serializer(data=item)

            if not serializer.is_valid():
                errors.update({
                    '{}-{}'.format(key, item['idx']): value
                    for key, value in serializer.errors.items()
                })

        if errors:
            return response.Response(errors, status.HTTP_400_BAD_REQUEST)
        else:
            return response.Response({})

    @list_route(methods=['post'])
    def validate(self, request):
        if isinstance(request.data, list):
            return self._validate_list(request.data)

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            return response.Response({})
        else:
            return response.Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
