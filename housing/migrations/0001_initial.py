# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2017-11-22 07:53
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('families', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BuildingElectricity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'building electricity',
                'verbose_name_plural': 'building electricity',
            },
        ),
        migrations.CreateModel(
            name='BuildingType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'building type',
                'verbose_name_plural': 'building types',
            },
        ),
        migrations.CreateModel(
            name='DrainageType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'drainage type',
                'verbose_name_plural': 'drainage types',
            },
        ),
        migrations.CreateModel(
            name='FamilyHousing',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ownership_status', models.CharField(blank=True, choices=[('owned', 'Owned'), ('rented', 'Rented'), ('mortgaged', 'Mortgaged'), ('squatter', 'Squatter'), ('informal-settler', 'Informal Settler')], default='', help_text='Status of ownership of the Home/Lot occupied', max_length=50)),
                ('distance_from_brgy_hall', models.CharField(blank=True, help_text='Location (distance from the Barangay Hall)', max_length=50)),
                ('gps_lat', models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True)),
                ('gps_lon', models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True)),
                ('home_settings', models.CharField(blank=True, choices=[('rural-community', 'Rural Community'), ('village-sitio', 'Village / Sitio'), ('semi-urban', 'Semi-Urban'), ('urban', 'Urban')], default='', max_length=50)),
                ('flood_frequency', models.CharField(blank=True, choices=[('none', 'None'), ('once', 'Once'), ('twice', 'Twice'), ('thrice', 'Thrice'), ('four-or-more', 'Four or More Times')], default='', help_text='Flood frequency in the last 7 years', max_length=50)),
                ('earthquake_frequency', models.CharField(blank=True, choices=[('none', 'None'), ('once', 'Once'), ('twice', 'Twice'), ('thrice', 'Thrice'), ('four-or-more', 'Four or More Times')], default='', help_text='Earthquake frequency in the last 7 years', max_length=50)),
                ('landslide_frequency', models.CharField(blank=True, choices=[('none', 'None'), ('once', 'Once'), ('twice', 'Twice'), ('thrice', 'Thrice'), ('four-or-more', 'Four or More Times')], default='', help_text='Landslide frequency in the last 7 years', max_length=50)),
                ('tornado_frequency', models.CharField(blank=True, choices=[('none', 'None'), ('once', 'Once'), ('twice', 'Twice'), ('thrice', 'Thrice'), ('four-or-more', 'Four or More Times')], default='', help_text='Tornado frequency in the last 7 years', max_length=50)),
                ('storm_surge', models.CharField(blank=True, choices=[('none', 'None'), ('once', 'Once'), ('twice', 'Twice'), ('thrice', 'Thrice'), ('four-or-more', 'Four or More Times')], default='', help_text='Storm Surge in the last 7 years', max_length=50)),
                ('vehicular_accident', models.CharField(blank=True, choices=[('none', 'None'), ('once', 'Once'), ('twice', 'Twice'), ('thrice', 'Thrice'), ('four-or-more', 'Four or More Times')], default='', help_text='Vehicular Accident in the last 7 years', max_length=50)),
                ('armed_conflict', models.CharField(blank=True, choices=[('none', 'None'), ('once', 'Once'), ('twice', 'Twice'), ('thrice', 'Thrice'), ('four-or-more', 'Four or More Times')], default='', help_text='Armed Conflict in the last 7 years', max_length=50)),
                ('disease_outbreak', models.CharField(blank=True, choices=[('none', 'None'), ('once', 'Once'), ('twice', 'Twice'), ('thrice', 'Thrice'), ('four-or-more', 'Four or More Times')], default='', help_text='Disease Outbreak in the last 7 years', max_length=50)),
                ('electricity_others', models.CharField(blank=True, max_length=50)),
                ('access_road_distance', models.CharField(blank=True, help_text='Distance to access road / passable all year round', max_length=50)),
                ('home_to_farm_distance', models.CharField(blank=True, help_text='Distance from house to production area / farm', max_length=50)),
                ('farm_to_road', models.CharField(blank=True, help_text='Distance from farm to access road / passable all year round', max_length=50)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('building_electricity', models.ManyToManyField(help_text='Electricity in the Building/House', to='housing.BuildingElectricity')),
                ('building_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='housing.BuildingType')),
                ('drainage', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='housing.DrainageType')),
                ('family', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='families.Family')),
            ],
            options={
                'verbose_name': "family's housing",
                'verbose_name_plural': "family's housing",
            },
        ),
        migrations.CreateModel(
            name='GarbageDisposal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'garbage disposal',
                'verbose_name_plural': 'garbage disposals',
            },
        ),
        migrations.CreateModel(
            name='HouseLocationRisk',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name': 'vulnerabilites and risks of house location',
            },
        ),
        migrations.CreateModel(
            name='ProductTransportation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'means of product transportation',
                'verbose_name_plural': 'means of product transportation',
            },
        ),
        migrations.CreateModel(
            name='RoofMaterial',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'roof material',
                'verbose_name_plural': 'roof materials',
            },
        ),
        migrations.CreateModel(
            name='ToiletType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'toilet type',
                'verbose_name_plural': 'toilet types',
            },
        ),
        migrations.CreateModel(
            name='WallingMaterial',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'walling material',
                'verbose_name_plural': 'walling materials',
            },
        ),
        migrations.CreateModel(
            name='WaterSource',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'water source',
                'verbose_name_plural': 'water sources',
            },
        ),
        migrations.AddField(
            model_name='familyhousing',
            name='garbage_disposal',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='housing.GarbageDisposal'),
        ),
        migrations.AddField(
            model_name='familyhousing',
            name='product_transportations',
            field=models.ManyToManyField(help_text='Means of Product Transportation', to='housing.ProductTransportation'),
        ),
        migrations.AddField(
            model_name='familyhousing',
            name='roof_material',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='housing.RoofMaterial'),
        ),
        migrations.AddField(
            model_name='familyhousing',
            name='toilet',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='housing.ToiletType'),
        ),
        migrations.AddField(
            model_name='familyhousing',
            name='vulnerability_and_risks',
            field=models.ManyToManyField(to='housing.HouseLocationRisk'),
        ),
        migrations.AddField(
            model_name='familyhousing',
            name='walling_material',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='housing.WallingMaterial'),
        ),
        migrations.AddField(
            model_name='familyhousing',
            name='water_source',
            field=models.ManyToManyField(to='housing.WaterSource'),
        ),
    ]
