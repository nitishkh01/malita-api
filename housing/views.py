from rest_framework import (
    viewsets,
)

from .models import (
    FamilyHousing,
)
from .serializers import (
    FamilyHousingSerializer,
    FamilyHousingWriteSerializer,
)


class FamilyHousingViewSet(viewsets.ModelViewSet):
    queryset = FamilyHousing.objects.select_related(
        'roof_material',
        'walling_material',
        'building_type',
        'drainage',
        'toilet',
        'garbage_disposal',
    ).prefetch_related(
        'vulnerability_and_risks',
        'building_electricity',
        'water_source',
        'product_transportations',
    )
    serializer_class = FamilyHousingSerializer

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return FamilyHousingSerializer
        else:
            return FamilyHousingWriteSerializer
