from rest_framework import serializers

from .models import (
    FamilyHousing,
    HouseLocationRisk,
    RoofMaterial,
    WallingMaterial,
    BuildingType,
    BuildingElectricity,
    WaterSource,
    DrainageType,
    ToiletType,
    GarbageDisposal,
    ProductTransportation,
)


class HouseLocationRiskSerializer(serializers.ModelSerializer):
    class Meta:
        model = HouseLocationRisk
        fields = '__all__'


class ProductTransportationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductTransportation
        fields = '__all__'


class RoofMaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoofMaterial
        fields = '__all__'


class WallingMaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = WallingMaterial
        fields = '__all__'


class BuildingTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildingType
        fields = '__all__'


class BuildingElectricitySerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildingElectricity
        fields = '__all__'


class WaterSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = WaterSource
        fields = '__all__'


class DrainageTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DrainageType
        fields = '__all__'


class ToiletTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToiletType
        fields = '__all__'


class GarbageDisposalSerializer(serializers.ModelSerializer):
    class Meta:
        model = GarbageDisposal
        fields = '__all__'


class FamilyHousingSerializer(serializers.ModelSerializer):
    roof_material = RoofMaterialSerializer()
    walling_material = WallingMaterialSerializer()
    building_type = BuildingTypeSerializer()
    drainage = DrainageTypeSerializer()
    toilet = ToiletTypeSerializer()
    garbage_disposal = GarbageDisposalSerializer()
    water_source = WaterSourceSerializer(many=True)
    vulnerability_and_risks = HouseLocationRiskSerializer(many=True)
    building_electricity = BuildingElectricitySerializer(many=True)
    product_transportations = ProductTransportationSerializer(many=True)

    class Meta:
        model = FamilyHousing
        fields = '__all__'


class FamilyHousingWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = FamilyHousing
        fields = '__all__'


class FamilyVulnerabilityAndRisksSerializer(serializers.ModelSerializer):
    vulnerability_and_risks = HouseLocationRiskSerializer(many=True)

    class Meta:
        model = FamilyHousing
        fields = ['vulnerability_and_risks']

    def get_vulnerability_and_risks(self, obj):
        return obj.vulnerability_and_risks.name if obj.vulnerability_and_risks.name else None
