from django.db import models

from families.constants import (
    OWNERSHIP_CHOICES,
    HOME_CHOICES,
    FREQ_CHOICES,
    DRAINAGE_CHOICES,
)


class HouseLocationRisk(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'vulnerability and risk of house location'
        verbose_name = 'vulnerabilites and risks of house location'

    def __str__(self):
        return self.name


class RoofMaterial(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'roof material'
        verbose_name_plural = 'roof materials'

    def __str__(self):
        return self.name


class WallingMaterial(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'walling material'
        verbose_name_plural = 'walling materials'

    def __str__(self):
        return self.name


class BuildingType(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'building type'
        verbose_name_plural = 'building types'

    def __str__(self):
        return self.name


class BuildingElectricity(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'building electricity'
        verbose_name_plural = 'building electricity'

    def __str__(self):
        return self.name


class WaterSource(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'water source'
        verbose_name_plural = 'water sources'

    def __str__(self):
        return self.name


class DrainageType(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'drainage type'
        verbose_name_plural = 'drainage types'

    def __str__(self):
        return self.name


class ToiletType(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'toilet type'
        verbose_name_plural = 'toilet types'

    def __str__(self):
        return self.name


class GarbageDisposal(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'garbage disposal'
        verbose_name_plural = 'garbage disposals'

    def __str__(self):
        return self.name


class ProductTransportation(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'means of product transportation'
        verbose_name_plural = 'means of product transportation'

    def __str__(self):
        return self.name


class FamilyHousing(models.Model):
    family = models.OneToOneField('families.Family', related_name='housing')
    ownership_status = models.CharField(
        max_length=100, blank=True, default='', choices=OWNERSHIP_CHOICES,
        help_text='Status of ownership of the Home/Lot occupied'
    )
    distance_from_brgy_hall = models.CharField(
        max_length=100, blank=True, help_text='Location (distance from the Barangay Hall)'
    )
    gps_lat = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    gps_lon = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    home_settings = models.CharField(max_length=100, blank=True, default='', choices=HOME_CHOICES)
    vulnerability_and_risks = models.ManyToManyField('HouseLocationRisk')
    flood_frequency = models.CharField(
        max_length=100, blank=True, default='', choices=FREQ_CHOICES,
        help_text='Flood frequency in the last 7 years'
    )
    earthquake_frequency = models.CharField(
        max_length=100, blank=True, default='', choices=FREQ_CHOICES,
        help_text='Earthquake frequency in the last 7 years'
    )
    landslide_frequency = models.CharField(
        max_length=100, blank=True, default='', choices=FREQ_CHOICES,
        help_text='Landslide frequency in the last 7 years'
    )
    tornado_frequency = models.CharField(
        max_length=100, blank=True, default='', choices=FREQ_CHOICES,
        help_text='Tornado frequency in the last 7 years'
    )
    storm_surge = models.CharField(
        max_length=100, blank=True, default='', choices=FREQ_CHOICES,
        help_text='Storm Surge in the last 7 years'
    )
    vehicular_accident = models.CharField(
        max_length=100, blank=True, default='', choices=FREQ_CHOICES,
        help_text='Vehicular Accident in the last 7 years'
    )
    armed_conflict = models.CharField(
        max_length=100, blank=True, default='', choices=FREQ_CHOICES,
        help_text='Armed Conflict in the last 7 years'
    )
    disease_outbreak = models.CharField(
        max_length=100, blank=True, default='', choices=FREQ_CHOICES,
        help_text='Disease Outbreak in the last 7 years'
    )
    roof_material = models.ForeignKey('RoofMaterial', blank=True, null=True)
    walling_material = models.ForeignKey('WallingMaterial', blank=True, null=True)
    building_type = models.ForeignKey('BuildingType', blank=True, null=True)
    building_electricity = models.ManyToManyField(
        'BuildingElectricity', help_text='Electricity in the Building/House'
    )
    electricity_others = models.CharField(max_length=100, blank=True)
    water_source = models.ManyToManyField('WaterSource')
    drainage = models.ForeignKey('DrainageType', blank=True, null=True)
    toilet = models.ForeignKey('ToiletType', blank=True, null=True)
    garbage_disposal = models.ForeignKey('GarbageDisposal', blank=True, null=True)
    access_road_distance = models.CharField(
        max_length=100, blank=True,
        help_text='Distance to access road / passable all year round'
    )
    home_to_farm_distance = models.CharField(
        max_length=100, blank=True,
        help_text='Distance from house to production area / farm'
    )
    farm_to_road = models.CharField(
        max_length=100, blank=True,
        help_text='Distance from farm to access road / passable all year round'
    )
    product_transportations = models.ManyToManyField(
        'ProductTransportation', help_text='Means of Product Transportation'
    )

    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "family's housing"
        verbose_name_plural = "family's housing"

    def __str__(self):
        return str(self.family)
