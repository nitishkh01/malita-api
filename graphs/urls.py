from django.conf.urls import include, url

from rest_framework.routers import DefaultRouter

from .views import (
    BarangayPopulationView,
    BloodTypeView,
    DisasterAndRiskView,
    GenderPerEducationLevelView,
    IncomeView,
    GenderView,
    GovernmentAidReceivedView,
)

router = DefaultRouter()

router.register(r'barangay-population', BarangayPopulationView, 'barangay-population')
router.register(r'blood-type', BloodTypeView, 'blood-type')
router.register(r'disaster-risk', DisasterAndRiskView, 'disaster-risk')
router.register(r'gender-education', GenderPerEducationLevelView, 'gender-education')
router.register(r'income', IncomeView, 'income')
router.register(r'government-aid-received', GovernmentAidReceivedView, 'government-aid-received')
router.register(r'gender', GenderView, 'gender')

urlpatterns = [
    url(r'^', include(router.urls)),
]
