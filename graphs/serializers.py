from rest_framework import serializers


class BarangayPopulationSerializer(serializers.Serializer):
    name = serializers.CharField(source='barangay__name')
    count = serializers.IntegerField()


class BloodTypeSerializer(serializers.Serializer):
    name = serializers.CharField()
    head = serializers.IntegerField()
    spouse = serializers.IntegerField()


class GenderSerializer(serializers.Serializer):
    name = serializers.CharField(source='head__gender')
    count = serializers.IntegerField()


class GenderDistributionSerializer(serializers.Serializer):
    name = serializers.CharField(source='head__highest_educational_attainment')
    female = serializers.IntegerField()
    male = serializers.IntegerField()


class GovernmentAidReceivedSerializer(serializers.Serializer):
    name = serializers.CharField(source='government_aid_received')
    count = serializers.IntegerField()


class IncomeSerializer(serializers.Serializer):
    order = serializers.IntegerField(source='familyincome__gross_monthly_income__order')
    name = serializers.CharField(source='familyincome__gross_monthly_income__value')
    count = serializers.IntegerField()


class VulnerabilityAndRiskSerializer(serializers.Serializer):
    name = serializers.CharField(
        source='housing__vulnerability_and_risks__name')
    count = serializers.IntegerField()
