from django.db.models import (
    Case,
    Count,
    When,
)

from rest_framework import viewsets

from core.views.mixins import PaginatedResponseMixin
from families import constants
from families.filters import FamilyFilter
from families.models import (
    Barangay,
    Family,
)
from income.models import GrossMonthlyIncome
from .serializers import (
    BarangayPopulationSerializer,
    BloodTypeSerializer,
    GenderSerializer,
    GenderDistributionSerializer,
    IncomeSerializer,
    GovernmentAidReceivedSerializer,
    VulnerabilityAndRiskSerializer,
)


class BarangayPopulationView(PaginatedResponseMixin, viewsets.ReadOnlyModelViewSet):
    serializer_class = BarangayPopulationSerializer
    filter_class = FamilyFilter
    queryset = Family.objects.with_barangays()

    def list(self, request):
        families = self.filter_queryset(self.get_queryset())
        families = Family.objects.filter(id__in=families.values_list('id', flat=True))
        families = families.values('barangay__name').annotate(
            count=Count('id')
        )

        existing_names = [item['barangay__name'] for item in families if item['barangay__name']]
        data = list(families)

        for name in Barangay.objects.exclude(name__in=existing_names).values_list('name', flat=True).distinct():
            data.append({
                'barangay__name': name,
                'count': 0,
            })

        data = sorted(data, key=lambda k: k['barangay__name'])
        return self.paginated_response(data)


class BloodTypeView(PaginatedResponseMixin, viewsets.ReadOnlyModelViewSet):
    serializer_class = BloodTypeSerializer
    filter_class = FamilyFilter
    queryset = Family.objects.all()

    def _get_count(self, blood_types, key, type_):
        for item in blood_types:
            if item[key] == type_:
                return item['count']
        return 0

    def list(self, request):
        families = self.filter_queryset(self.get_queryset())
        families = Family.objects.filter(id__in=families.values_list('id', flat=True))
        head_blood_types = families.values('head__blood_type').annotate(
            count=Count('id')
        )
        spouse_blood_types = families.values('spouse__blood_type').annotate(
            count=Count('id')
        )

        data = []
        for item in constants.BLOOD_TYPE_CHOICES:
            type_ = '' if item[0].lower() == 'undefined' else item[0]
            data.append({
                'name': item[1],
                'head': self._get_count(head_blood_types, 'head__blood_type', type_),
                'spouse': self._get_count(spouse_blood_types, 'spouse__blood_type', type_),
            })

        return self.paginated_response(data)


class GenderPerEducationLevelView(PaginatedResponseMixin, viewsets.ReadOnlyModelViewSet):
    serializer_class = GenderDistributionSerializer
    filter_class = FamilyFilter
    queryset = Family.objects.with_highest_educational_attainment()

    def list(self, request):
        families = self.filter_queryset(self.get_queryset())
        families = Family.objects.filter(id__in=families.values_list('id', flat=True))
        families = families.values(
            'head__highest_educational_attainment'
        ).annotate(
            female=Count(
                Case(
                    When(head__gender=constants.GENDER_FEMALE, then='id')
                )
            ),
            male=Count(
                Case(
                    When(head__gender=constants.GENDER_MALE, then='id')
                )
            ),
        )

        existing_names = [
            item['head__highest_educational_attainment']
            for item in families
            if item['head__highest_educational_attainment']
        ]
        data = list(families)

        for item in constants.EDUCATIONAL_ATTAINMENT_CHOICES:
            if item[0] not in existing_names:
                data.append({
                    'head__highest_educational_attainment': item[0],
                    'male': 0,
                    'female': 0,
                })

        data = sorted(data, key=lambda k: k['head__highest_educational_attainment'])
        return self.paginated_response(data)


class DisasterAndRiskView(PaginatedResponseMixin, viewsets.ReadOnlyModelViewSet):
    serializer_class = VulnerabilityAndRiskSerializer
    filter_class = FamilyFilter
    queryset = Family.objects.with_housing_risk()

    def list(self, request):
        families = self.filter_queryset(self.get_queryset())
        families = Family.objects.filter(id__in=families.values_list('id', flat=True))
        families = families.values('housing__vulnerability_and_risks__name').annotate(
            count=Count('id')
        )
        existing_types = [
            item['housing__vulnerability_and_risks__name']
            for item in families
            if item['housing__vulnerability_and_risks__name']
        ]
        data = list(families)

        for value in Family.objects.exclude(housing__vulnerability_and_risks__name__in=existing_types).values_list('housing__vulnerability_and_risks__name', flat=True).distinct():
            if value is not None:
                data.append({
                    'housing__vulnerability_and_risks__name': value,
                    'count': 0,
                })

        data = sorted(data, key=lambda k: k['housing__vulnerability_and_risks__name'])
        return self.paginated_response(data)


class GenderView(PaginatedResponseMixin, viewsets.ReadOnlyModelViewSet):
    serializer_class = GenderSerializer
    filter_class = FamilyFilter
    queryset = Family.objects.all()

    def list(self, request):
        families = self.filter_queryset(self.get_queryset())
        families = Family.objects.filter(id__in=families.values_list('id', flat=True))
        families = families.values('head__gender').annotate(
            count=Count('id')
        )

        existing_values = [
            item['head__gender']
            for item in families
        ]
        data = list(families)

        for value in ['', 'male', 'female']:
            if value not in existing_values:
                data.append({
                    'head__gender': value,
                    'count': 0
                })

        data = sorted(data, key=lambda k: k['head__gender'])
        return self.paginated_response(data)


class GovernmentAidReceivedView(PaginatedResponseMixin, viewsets.ReadOnlyModelViewSet):
    serializer_class = GovernmentAidReceivedSerializer
    filter_class = FamilyFilter
    queryset = Family.objects.with_government_aid()

    def list(self, request):
        families = self.filter_queryset(self.get_queryset())
        families = Family.objects.filter(id__in=families.values_list('id', flat=True))
        families = families.values('government_aid_received').annotate(
            count=Count('id')
        )

        existing_names = [item['government_aid_received'] for item in families]
        data = list(families)

        for item in Family.AID_RECEIVED_CHOICES:
            if item[0] not in existing_names:
                data.append({
                    'government_aid_received': item[0],
                    'count': 0,
                })

        data = sorted(data, key=lambda k: k['government_aid_received'])
        return self.paginated_response(data)


class IncomeView(PaginatedResponseMixin, viewsets.ReadOnlyModelViewSet):
    serializer_class = IncomeSerializer
    filter_class = FamilyFilter
    queryset = Family.objects.with_income()

    def list(self, request):
        families = self.filter_queryset(self.get_queryset())
        families = Family.objects.filter(id__in=families.values_list('id', flat=True))
        families = families.values('familyincome__gross_monthly_income__value').annotate(
            count=Count('id')
        ).values('familyincome__gross_monthly_income__order', 'familyincome__gross_monthly_income__value', 'count')

        existing_values = [
            item['familyincome__gross_monthly_income__value']
            for item in families
            if item['familyincome__gross_monthly_income__value']
        ]
        data = list(families)

        for income in GrossMonthlyIncome.objects.exclude(value__in=existing_values):
            data.append({
                'familyincome__gross_monthly_income__order': income.order,
                'familyincome__gross_monthly_income__value': income.value,
                'count': 0,
            })

        data = sorted(data, key=lambda k: k['familyincome__gross_monthly_income__order'])
        return self.paginated_response(data)
