import os

from django.conf import settings

from families.models import Family


with open(os.path.join(settings.BASE_DIR, 'scripts', 'points.txt')) as file_:
    for points in file_:
        family = Family.objects.with_barangays(barangay__name__icontains='tubalan', map_points='').first()
        family.map_points = points.strip()
        family.save(update_fields=['map_points'])
